/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package nl.grootligt.agenda;

import java.util.ArrayList;
import java.util.Date;
import nl.grootligt.parkeergarage.Auto;

/**
 * @author Rory
 */
public class Agenda {
    private ArrayList<Monteur> deMonteurs = new ArrayList<>();
    private ArrayList<Reparatie> deReparaties = new ArrayList<>();
    
    public Agenda(){
    }
    
    public void addReparatie(String status, String naam, Date begindatum, Auto auto){
        Reparatie r = new Reparatie(status, naam, begindatum, auto);
        deReparaties.add(r);
    }
    public boolean deleteReparatie(Reparatie r){
        if(r.isGereed() || r.delete())
            return false;
        
        deReparaties.remove(r);
        return true;
    }
    private boolean checkReparatie(Reparatie r){
        for(Reparatie value : deReparaties){
            if(value==r)
                return false;
        }
        return true;
    }
    
    public void addMonteur(String voornaam, String achternaam, Date geboortedatum){
        Monteur m = new Monteur(voornaam, achternaam, geboortedatum);
        deMonteurs.add(m);
    }
    
    public boolean deleteMonteur(Monteur m){
        if(checkMonteur(m))
            return false;
        for(Reparatie value : deReparaties){
            if(!value.isGereed())
                value.deleteMonteur(m);
        }
        
        return true;
    }
    
    private boolean checkMonteur(Monteur m){
        for(Monteur value : deMonteurs){
            if(value==m)
                return false;
        }
        return true;
    }
    
    public boolean koppelMonRep(Monteur m, Reparatie r){
        if(!checkReparatie(r) && !checkMonteur(m) && m.isActief()){
            r.addMonteur(m);
            return true;
        }
        return false;
    }
    
    public ArrayList<Reparatie> getReparaties() {
    	return deReparaties;
    }
    
    public ArrayList<Monteur> getMonteurs() {
    	return deMonteurs;
    }
    
    public Reparatie getReparatieByName(String naam) {
    	Reparatie r = null;
    	for(Reparatie rr: deReparaties) {
    		if(rr.getNaam().equals(naam)) {
    			r = rr;
    		}
    	}
    	return r;
    }
}