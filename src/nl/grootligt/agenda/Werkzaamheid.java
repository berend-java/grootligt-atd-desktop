/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package nl.grootligt.agenda;

import java.util.Date;

/**
 * @author Rory
 */
public class Werkzaamheid {
	private String naam;
    private String omschrijving;
    private Date begindatum, einddatum;
    private boolean gereed = false;

    public Werkzaamheid(String naam, String omschrijving, Date begindatum) {
    	this.setNaam(naam);
        this.omschrijving = omschrijving;
        this.begindatum = begindatum;
    }

    public boolean finish(){
        if(gereed)
            return false;
        einddatum = new Date();
        gereed = true;
        return true;
    }
    
    public String getOmschrijving() {
        return omschrijving;
    }

    public void setOmschrijving(String omschrijving) {
        this.omschrijving = omschrijving;
    }

    public Date getBegindatum() {
        return begindatum;
    }

    public void setBegindatum(Date begindatum) {
        this.begindatum = begindatum;
    }

    public Date getEinddatum() {
        return einddatum;
    }

    public void setEinddatum(Date einddatum) {
        this.einddatum = einddatum;
    }

    public boolean isGereed() {
        return gereed;
    }

    public void setGereed(boolean gereed) {
        this.gereed = gereed;
    }

	public String getNaam() {
		return naam;
	}

	public void setNaam(String naam) {
		this.naam = naam;
	}
}
