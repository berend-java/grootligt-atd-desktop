/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package nl.grootligt.agenda;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import nl.grootligt.parkeergarage.Auto;
import nl.grootligt.voorraadbeheer.Brandstof;
import nl.grootligt.voorraadbeheer.Onderdeel;

/**
 * @author Rory
 */
public class Reparatie {
    private String status, naam;
    private Date begindatum, einddatum;
    private boolean gereed = false;
    
    private ArrayList<Werkzaamheid> deWerkzaamheden = new ArrayList<>();
    private ArrayList<Monteur> deMonteurs = new ArrayList<>();
    private Auto deAuto;
    private ArrayList<Brandstof> deBrandstoffen = new ArrayList<>();
    private ArrayList<Onderdeel> deOnderdelen = new ArrayList<>();

    public Reparatie(String status, String naam, Date begindatum, Auto auto) {
        this.status = status;
        this.naam = naam;
        this.begindatum = begindatum;
        if(auto.isActief())
            deAuto = auto;
    }
    
//    public int getManuur(){
//        return deMonteurs.size() * (einddatum - begindatum) * 8;
//    }
    
    public boolean finish(){
        if(gereed)
            return false;
        gereed = true;
        einddatum = new Date();
        status = "klaar";
        for(Monteur m : deMonteurs)
            m.setBezet(false);
        
        if(naam.equals("Onderhoud"))
            deAuto.setLaastOnderhoud(new Date());
        
        deAuto.getDeEigenaar().setLaatstGeweest(new Date());
        
        return true;
    }
    
    public boolean delete(){
        for(Werkzaamheid value : getDeWerkzaamheden()){
            if(value.isGereed())
                return false;
        }
        if(isGereed())
            return false;
        for(Werkzaamheid value2 : deWerkzaamheden){
            for(Monteur value3 : deMonteurs){
                value3.deleteWerkzaamheid(value2);
            }
        }
        deWerkzaamheden.clear();
        for(Monteur value : deMonteurs){
            value.setBezet(false);
        }
        deMonteurs.clear();
        return true;
    }
    
    public boolean addOnderdeel(Onderdeel o){
        if(checkOnderdeel(o)){
            deOnderdelen.add(o);
            return true;
        }
        return false;
    }
    public boolean addBrandstof(Brandstof b){
        if(checkBrandstof(b)){
            deBrandstoffen.add(b);
            return true;
        }
        return false;
    }
    
    private boolean checkOnderdeel(Onderdeel o){
        for(Onderdeel value : deOnderdelen){
            if(value==o){
                return false;
            }
        }
        return true;
    }
    private boolean checkBrandstof(Brandstof b){
        for(Brandstof value : deBrandstoffen){
            if(value==b){
                return false;
            }
        }
        return true;
    }

    public boolean addMonteur(Monteur m){
        if(checkMonteur(m) && !m.isBezet() && m.isActief()){
            deMonteurs.add(m);
            return true;
        }
        return false;
    }
    public boolean deleteMonteur(Monteur m){
        if(checkMonteur(m) || isGereed())
            return false;
        
        for(Werkzaamheid value : deWerkzaamheden){
            for(Werkzaamheid value2 : m.getDeWerkzaamheden()){
                if(value==value2)
                    m.deleteWerkzaamheid(value2);
            }
        }
        m.setBezet(false);
        deMonteurs.remove(m);
        
        return true;
    }
    
    private boolean checkMonteur(Monteur m){
        for(Monteur value : deMonteurs){
            if(value==m)
                return false;
        }
        return true;
    }
    
    public void addWerkzaamheid(String naam, String omschrijving, Date begindatum){
        deWerkzaamheden.add(new Werkzaamheid(naam, omschrijving, begindatum));
    }
    public boolean deleteWerkzaamheid(Werkzaamheid w){
        if(w.isGereed())
            return false;
        
        for(Monteur value : deMonteurs){
            value.deleteWerkzaamheid(w);
        }
        deWerkzaamheden.remove(w);
        
        return true;
    }
    
    private boolean checkWerkzaamheid(Werkzaamheid w){
        for(Werkzaamheid value : deWerkzaamheden){
            if(value==w)
                return false;
        }
        return true;
    }
    
    public boolean koppelMonWerk(Monteur m, Werkzaamheid w){
        if(!checkMonteur(m) && !checkWerkzaamheid(w) && m.isActief()){
            m.addWerkzaamheid(w);
            return true;
        }
        return false;
    }
    
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getBegindatum() {
        return begindatum;
    }

    public void setBegindatum(Date begindatum) {
        this.begindatum = begindatum;
    }

    public Date getEinddatum() {
        return einddatum;
    }

    public void setEinddatum(Date einddatum) {
        this.einddatum = einddatum;
    }

    public ArrayList<Werkzaamheid> getDeWerkzaamheden() {
        return deWerkzaamheden;
    }

    public ArrayList<Monteur> getDeMonteurs() {
        return deMonteurs;
    }
    
    public String getMonteursString() {
    	String s = "";
    	for(Monteur m: deMonteurs) {
    		s += m.getVoornaam() + " ";
    	}
    	return s;
    }

    public String getNaam() {
        return naam;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    public boolean isGereed() {
        return gereed;
    }

    public void setGereed(boolean gereed) {
        this.gereed = gereed;
    }

    public Auto getDeAuto() {
        return deAuto;
    }

    public ArrayList<Brandstof> getDeBrandstoffen() {
        return deBrandstoffen;
    }

    public ArrayList<Onderdeel> getDeOnderdelen() {
        return deOnderdelen;
    }
    
    public int getAantalOnderdelen() {
    	return deOnderdelen.size();
    }
    
    public int getAaantalBrandstof() {
    	return deBrandstoffen.size();
    }
    
    public String getBegindatumString() {
    	DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
    	String s = df.format(begindatum);
    	return s;
    }
    
    public String toString() {
    	return naam;
    }
}
