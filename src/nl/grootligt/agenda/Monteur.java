/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package nl.grootligt.agenda;

import java.util.ArrayList;
import java.util.Date;

/**
 * @author Rory
 */
public class Monteur {
    private String voornaam, achternaam;
    private Date geboortedatum;
    private boolean bezet = false, actief = true;
    
    private ArrayList<Werkzaamheid> deWerkzaamheden = new ArrayList<>();

    public Monteur(String voornaam, String achternaam, Date geboortedatum) {
        this.voornaam = voornaam;
        this.achternaam = achternaam;
        this.geboortedatum = geboortedatum;
    }
    
    public boolean addWerkzaamheid(Werkzaamheid w){
        if(!actief || w.isGereed())
            return false;
        if(checkWerkzaamheid(w)){
            deWerkzaamheden.add(w);
            return true;
        }
        return false;
    }
    
    public boolean deleteWerkzaamheid(Werkzaamheid w){
        if(!checkWerkzaamheid(w) || w.isGereed())
            return false;
        deWerkzaamheden.remove(w);
        return true;
    }
    
    private boolean checkWerkzaamheid(Werkzaamheid w){
        for(Werkzaamheid value : deWerkzaamheden){
            if(value==w)
                return false;
        }
        return true;
    }

    public String getVoornaam() {
        return voornaam;
    }

    public void setVoornaam(String voornaam) {
        this.voornaam = voornaam;
    }

    public String getAchternaam() {
        return achternaam;
    }

    public void setAchternaam(String achternaam) {
        this.achternaam = achternaam;
    }

    public Date getGeboortedatum() {
        return geboortedatum;
    }

    public void setGeboortedatum(Date geboortedatum) {
        this.geboortedatum = geboortedatum;
    }

    public boolean isBezet() {
        return bezet;
    }

    public void setBezet(boolean bezet) {
        this.bezet = bezet;
    }

    public ArrayList<Werkzaamheid> getDeWerkzaamheden() {
        return deWerkzaamheden;
    }
    
    public boolean isActief() {
        return actief;
    }

    public void setActief(boolean actief) {
        this.actief = actief;
    }
    
    public String toString() {
    	return voornaam + " " + achternaam;
    }
}
