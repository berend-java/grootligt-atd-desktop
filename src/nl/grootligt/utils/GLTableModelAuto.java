package nl.grootligt.utils;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import nl.grootligt.parkeergarage.Auto;

public class GLTableModelAuto extends AbstractTableModel{
	/**
	 * @author Berend de Groot
	 */
	private static final long serialVersionUID = 1L;
	private String[] columnNames;
	private ArrayList<Auto> data;
	
	public GLTableModelAuto(String[] s, ArrayList<Auto> d) {
		columnNames = s;
		data = d;
	}
		
	public int getColumnCount() {
		return columnNames.length;
	}

	public int getRowCount() {
		return data.size();
	}

	public String getColumnName(int col) {
	    return columnNames[col];
	}

	public Object getValueAt(int rowIndex, int columnIndex) {
		Auto b = data.get(rowIndex);
        switch (columnIndex) {
        	case 0: return b.getKenteken();
        	case 1: return b.getMerk();
        	case 2: return b.getType();
        	case 3: return b.getDeEigenaar();
        	case 4: return b.getBrandstof();
        }
        return null;
	}
}
