package nl.grootligt.utils;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import nl.grootligt.klantenbinding.Klant;

public class GLTableModelKlant extends AbstractTableModel{
	/**
	 * @author Berend de Groot
	 */
	private static final long serialVersionUID = 1L;
    private String[] columnNames;
	private ArrayList<Klant> data;
	
	public GLTableModelKlant(String[] s, ArrayList<Klant> d) {
		columnNames = s;
		data = d;
	}
		
	public int getColumnCount() {
		return columnNames.length;
	}

	public int getRowCount() {
		return data.size();
	}

	public String getColumnName(int col) {
	    return columnNames[col];
	}

	public Object getValueAt(int rowIndex, int columnIndex) {
        Klant k = data.get(rowIndex);
        switch (columnIndex) {
        	case 0: return k.getVoornaam();
        	case 1: return k.getAchternaam();
        	case 2: return k.getEmail();
        	case 3: return k.getGeboorteDatumString();
        	case 4: return k.getAantalAutos();
        }
        return null;
	}
}
