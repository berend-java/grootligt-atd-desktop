package nl.grootligt.utils;

import java.awt.Color;

public class GLConfiguratie {
	/**
	 * @author Berend de Groot
	 */
	private static Color mFrameBackground = new Color(71, 71, 71);
	private static Color mFrameForeground = new Color(49, 49, 49);
	private static Color mButtonForeground = new Color(94, 160 ,2);
	
	private static Color vButtonBackground = new Color(49, 49, 49);
	private static Color vButtonForeground = Color.WHITE;
	private static Color vTableBackgrid = new Color(94, 160, 2);

	public static Color getFrameBackground() {
		return mFrameBackground;
	}

	public static void setFrameBackground(Color backgroundColor) {
		GLConfiguratie.mFrameBackground = backgroundColor;
	}

	public static Color getFrameForeground() {
		return mFrameForeground;
	}

	public static void setFrameForeground(Color buttonBackground) {
		GLConfiguratie.mFrameForeground = buttonBackground;
	}

	public static Color getButtonForegound() {
		return mButtonForeground;
	}

	public static void setButtonForeground(Color buttonForeground) {
		GLConfiguratie.mButtonForeground = buttonForeground;
	}

	public static Color getvButtonBackground() {
		return vButtonBackground;
	}

	public static void setvButtonBackground(Color vButtonBackground) {
		GLConfiguratie.vButtonBackground = vButtonBackground;
	}

	public static Color getvButtonForeground() {
		return vButtonForeground;
	}

	public static void setvButtonForeground(Color vButtonForeground) {
		GLConfiguratie.vButtonForeground = vButtonForeground;
	}

	public static Color getvTableBackgrid() {
		return vTableBackgrid;
	}

	public static void setvTableBackgrid(Color vTableBackgrid) {
		GLConfiguratie.vTableBackgrid = vTableBackgrid;
	}
}
