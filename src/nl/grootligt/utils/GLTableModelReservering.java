package nl.grootligt.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.table.AbstractTableModel;

import nl.grootligt.parkeergarage.Reservering;

public class GLTableModelReservering extends AbstractTableModel{
	/**
	 * @author Berend de Groot
	 */
	private static final long serialVersionUID = 1L;
	private String[] columnNames;
	private ArrayList<Reservering> data;
	
	public GLTableModelReservering(String[] s, ArrayList<Reservering> d) {
		columnNames = s;
		data = d;
	}
		
	public int getColumnCount() {
		return columnNames.length;
	}

	public int getRowCount() {
		return data.size();
	}

	public String getColumnName(int col) {
	    return columnNames[col];
	}

	public Object getValueAt(int rowIndex, int columnIndex) {
		Reservering b = data.get(rowIndex);
        switch (columnIndex) {
        	case 0: 
        	   	DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        		Date datum = b.getStartdatum();
        		String sDatum = df.format(datum);
        		return sDatum;
        	case 1: return b.getDeAuto();
        	case 2: return b.getDePlek();
        }
        return null;
	}
}
