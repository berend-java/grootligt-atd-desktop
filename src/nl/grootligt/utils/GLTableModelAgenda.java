package nl.grootligt.utils;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import nl.grootligt.agenda.Reparatie;

public class GLTableModelAgenda extends AbstractTableModel{
	/**
	 * @author Berend de Groot
	 */
	private static final long serialVersionUID = 1L;
    private String[] columnNames;
	private ArrayList<Reparatie> data;
	
	public GLTableModelAgenda(String[] s, ArrayList<Reparatie> d) {
		columnNames = s;
		data = d;
	}
		
	public int getColumnCount() {
		return columnNames.length;
	}

	public int getRowCount() {
		return data.size();
	}

	public String getColumnName(int col) {
	    return columnNames[col];
	}

	public Object getValueAt(int rowIndex, int columnIndex) {
        Reparatie r = data.get(rowIndex);
        switch (columnIndex) {
        	case 0: return r.getNaam();
        	case 1: return r.getMonteursString();
        	case 2: return r.getAantalOnderdelen();
        	case 3: return r.getAaantalBrandstof();
        	case 4: return r.getStatus();
        }
        return null;
	}
}
