package nl.grootligt.utils;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import nl.grootligt.voorraadbeheer.Onderdeel;

public class GLTableModelOnderdeel extends AbstractTableModel{
	/**
	 * @author Berend de Groot
	 */
	private static final long serialVersionUID = 1L;
    private String[] columnNames;
	private ArrayList<Onderdeel> data;
	
	public GLTableModelOnderdeel(String[] s, ArrayList<Onderdeel> d) {
		columnNames = s;
		data = d;
	}
		
	public int getColumnCount() {
		return columnNames.length;
	}

	public int getRowCount() {
		return data.size();
	}

	public String getColumnName(int col) {
	    return columnNames[col];
	}

	public Object getValueAt(int rowIndex, int columnIndex) {
        Onderdeel o = data.get(rowIndex);
        switch (columnIndex) {
        	case 0: return o.getArtikelnummer();
        	case 1: return o.getAantal();
        	case 2: return o.getPrijs();
        	case 3: return o.getType();
        }
        return null;
	}
}
