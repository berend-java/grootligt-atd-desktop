package nl.grootligt.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import nl.grootligt.agenda.Agenda;
import nl.grootligt.facturatie.Deurwaarder;
import nl.grootligt.klantenbinding.Klandizie;
import nl.grootligt.klantenbinding.Klant;
import nl.grootligt.parkeergarage.Auto;
import nl.grootligt.parkeergarage.Garage;
import nl.grootligt.voorraadbeheer.Magazijn;

public class GLTestData {
	/**
	 * @author Berend de Groot
	 */
	private Magazijn m;
	private Klandizie k;
	private Garage g;
	private Agenda a;
	private Deurwaarder d;
	
	public GLTestData(Magazijn m, Klandizie k, Garage g, Agenda a, Deurwaarder d) {
		this.m = m;
		this.k = k;
		this.g = g;
		this.a = a;
		this.d = d;
	}
	
	public void pushAutos() {
		g.addAuto("S80", "Volvo", "Benzine", "10-FF-D1J");
		g.addAuto("S40", "Volvo", "Benzine", "12-DC-123");
		g.addAuto("Benz", "Mercedes", "Diesel", "LOL-LOL-G");
		g.addAuto("R8", "Audi", "Diesel", "GL-BDG-RVL");
	}
	
	public void pushKlanten() {
		Date date = null, date2 = null, date3 = null, date4 = null, date5 = null, date6 = null, date7 = null, date8 = null;
		try {
			date = new SimpleDateFormat("dd-MM-yyyy").parse("04-06-1995");
			date2 = new SimpleDateFormat("dd-MM-yyyy").parse("09-12-1992");
			date3 = new SimpleDateFormat("dd-MM-yyyy").parse("04-02-1979");
			date4 = new SimpleDateFormat("dd-MM-yyyy").parse("10-12-1988");
			date5 = new SimpleDateFormat("dd-MM-yyyy").parse("26-08-1966");
			date6 = new SimpleDateFormat("dd-MM-yyyy").parse("25-11-1932");
			date7 = new SimpleDateFormat("dd-MM-yyyy").parse("12-12-1972");
			date8 = new SimpleDateFormat("dd-MM-yyyy").parse("05-01-1999");
		} catch (ParseException e) {
			e.printStackTrace();
		}
		k.addKlant("Berend", "de Groot", "berend@b3r3nd.nl", date);
		k.addKlant("Rory", "van Ligten", "rvl@live.nl", date2);
		k.addKlant("Bart", "van Eijkelenburg", "bart.vaneijkelenburg@hu.nl", date3);
		k.addKlant("Melissa", "de Groot", "mel_degroot@b3r3nd.nl", date4);
		k.addKlant("Marc", "Verhage", "marcv@hotmail.com", date5);
		k.addKlant("Johan", "vScholten", "scholten.johan@bm.nl", date6);
		k.addKlant("Klaas", "Bakker", "klaasbakker@hotmail.com", date7);
		k.addKlant("Piet", "van Oosterom", "none", date8);
		
		for(Klant kk: k.getDeKlanten()) {
			if(!kk.getVoornaam().equals("Berend")) {
				kk.setLaatstGeweest(date5);
			}
		}
		
	}
	
	public void pushOnderdelen() {
		m.addOnderdeel(1, 5, 20.0, "Band Klein");
		m.addOnderdeel(2, 5, 25.0, "Band Middel");
		m.addOnderdeel(3, 5, 35.0, "Band Groot");
		m.addOnderdeel(4, 14, 99.99, "Raam");
		m.addOnderdeel(5, 22, 149.50, "Deur");
		m.addOnderdeel(6, 18, 70.0, "Velg");
		m.addOnderdeel(7, 44, 15.0, "Slot");
		m.addOnderdeel(8, 54, 30.0, "Lampen voor");
		m.addOnderdeel(9, 12, 30.0, "Lampen achter");
		m.addOnderdeel(10, 4, 54.0, "Stuur");
		m.addOnderdeel(11, 2, 120.50, "Stoelen");
		m.addOnderdeel(12, 7, 399.99, "Motor");
	}
	
	public void pushReparaties() {
		a.addReparatie("Bezig", "Reparatie 1", new Date(), new Auto("S81", "Volvo", "Benzine", "10-FF-D1J"));
		a.addReparatie("Bezig", "Reparatie 2", new Date(), new Auto("S82", "Volvo", "Benzine", "10-FF-D1J"));
	}
	
	public void pushFacturen() {
		d.addParkFact(null);
		d.addRepFact(null);
		d.addTankFact(null);
	}
	
	public void pushMonteurs() {
		a.addMonteur("Berend", "de Groot", new Date());
	}
}
