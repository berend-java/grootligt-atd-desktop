package nl.grootligt.utils;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import nl.grootligt.parkeergarage.Plek;
import nl.grootligt.voorraadbeheer.Brandstof;

public class GLTableModelPlek extends AbstractTableModel{
	/**
	 * @author Berend de Groot
	 */
	private static final long serialVersionUID = 1L;
	private String[] columnNames;
	private ArrayList<Plek> data;
	
	public GLTableModelPlek(String[] s, ArrayList<Plek> d) {
		columnNames = s;
		data = d;
	}
		
	public int getColumnCount() {
		return columnNames.length;
	}

	public int getRowCount() {
		return data.size();
	}

	public String getColumnName(int col) {
	    return columnNames[col];
	}

	public Object getValueAt(int rowIndex, int columnIndex) {
		Plek b = data.get(rowIndex);
        switch (columnIndex) {
        	case 0: return b.getPleknummer();
        	case 1: return b.isBezet();
        	case 2: return b.isSchoon();
        	case 3: return b.getPrijs();
        }
        return null;
	}
}
