package nl.grootligt.utils;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import nl.grootligt.voorraadbeheer.Brandstof;

public class GLTableModelBrandstof extends AbstractTableModel{
	/**
	 * @author Berend de Groot
	 */
	private static final long serialVersionUID = 1L;
	private String[] columnNames;
	private ArrayList<Brandstof> data;
	
	public GLTableModelBrandstof(String[] s, ArrayList<Brandstof> d) {
		columnNames = s;
		data = d;
	}
		
	public int getColumnCount() {
		return columnNames.length;
	}

	public int getRowCount() {
		return data.size();
	}

	public String getColumnName(int col) {
	    return columnNames[col];
	}

	public Object getValueAt(int rowIndex, int columnIndex) {
        Brandstof b = data.get(rowIndex);
        switch (columnIndex) {
        	case 0: return b.getTSIC();
        	case 1: return b.getAantalLiters();
        	case 2: return b.getPrijsPerLiter();
        	case 3: return b.getType();
        }
        return null;
	}
}
