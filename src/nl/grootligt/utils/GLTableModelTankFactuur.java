package nl.grootligt.utils;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;
import nl.grootligt.facturatie.TankFactuur;

public class GLTableModelTankFactuur extends AbstractTableModel{
	/**
	 * @author Berend de Groot
	 */
	private static final long serialVersionUID = 1L;
	private String[] columnNames;
	private ArrayList<TankFactuur> data;
	
	public GLTableModelTankFactuur(String[] s, ArrayList<TankFactuur> d) {
		columnNames = s;
		data = d;
	}
		
	public int getColumnCount() {
		return columnNames.length;
	}

	public int getRowCount() {
		return data.size();
	}

	public String getColumnName(int col) {
	    return columnNames[col];
	}

	public Object getValueAt(int rowIndex, int columnIndex) {
		TankFactuur r = data.get(rowIndex);
        switch (columnIndex) {
        	case 0: return r.getDatum();
        	case 1: return r.getBedrag();
        	case 2: return r.isBetaald();
        	case 3: return r.isGereed();
        }
        return null;
	}
}
