/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package nl.grootligt.klantenbinding;

import java.util.Date;

/**
 * @author Rory
 */
public abstract class Herinnering {
    protected Date datum;
    protected String bericht;
    protected boolean verstuurd = false;

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    public String getBericht() {
        return bericht;
    }

    public void setBericht(String bericht) {
        this.bericht = bericht;
    }

    public boolean isVerstuurd() {
        return verstuurd;
    }

    public void setVerstuurd(boolean verstuurd) {
        this.verstuurd = verstuurd;
    }
}
