/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package nl.grootligt.klantenbinding;

import java.util.ArrayList;
import java.util.Date;
import nl.grootligt.parkeergarage.Auto;

/**
 * @author Rory
 */
public class Klandizie {
    private ArrayList<Klant> deKlanten = new ArrayList<>();
    private ArrayList<Herinnering> deKlantMeldingen = new ArrayList<>();
    private ArrayList<Herinnering> deAutoMeldingen = new ArrayList<>();

    public Klandizie() {
    }
    
    public boolean koppelAutoKlant(Auto a, Klant k){
        if(a.getDeEigenaar()!=null && !a.isActief() && !k.isActief())
            return false;
        k.addAuto(a);
        a.setDeEigenaar(k);
        return true;
    }
    
    public ArrayList genereerAOMelding(){
        ArrayList<Auto> al = new ArrayList<>();
        
        Date d = new Date();
        d.setMonth(d.getMonth()-6);
        
        for(Klant value : deKlanten){
            for(Auto value2 : value.getDeAutos()){
                if(value2.getLaastOnderhoud().before(d) && value.isActief() && value2.isActief())
                    al.add(value2);
            }
        }
        
        return al;
    }
    public void sendAOMelding(ArrayList<Auto> al){
        for(Auto value : al)
            addAutoMelding(value);
    }
    
    public ArrayList genereerLNGMelding(){
        ArrayList<Klant> al = new ArrayList<>();
        
        Date d = new Date();
        d.setMonth(d.getMonth()-2);
        
        for(Klant value : deKlanten){
            if(value.getLaatstGeweest().before(d) && value.isActief())
                al.add(value);
        }
        
        return al;
    }
    public void sendLNGMelding(ArrayList<Klant> al){
        for(Klant value : al){
            addKlantMelding(value);
        }
    }
    
    public void addKlant(String voornaam, String achternaam, String email, Date geboortedatum){
        Klant k = new Klant(voornaam, achternaam, email, geboortedatum);
        deKlanten.add(k);
    }
    
    public boolean removeKlant(Klant k){
        if(!k.isActief())
            return false;
        if(klantAanwezig(k)){
            for(Auto value : k.getDeAutos())
                value.setDeEigenaar(null);
            k.setActief(false);
            return true;
        }
        return false;
    }
    
    private boolean klantAanwezig(Klant k){
        for(Klant value : deKlanten){
            if(value == k)
                return true;
        }
        return false;
    }
    
    private void addKlantMelding(Klant k){
        Herinnering h = new LangNietGeweest(k);
        deKlantMeldingen.add(h);
    }
    private void addAutoMelding(Auto a){
        Herinnering h = new AutoOnderhoud(a);
        deAutoMeldingen.add(h);
    }

    public ArrayList<Klant> getDeKlanten() {
        return deKlanten;
    }

    public ArrayList<Herinnering> getDeKlantMeldingen() {
        return deKlantMeldingen;
    }

    public ArrayList<Herinnering> getDeAutoMeldingen() {
        return deAutoMeldingen;
    }
}
