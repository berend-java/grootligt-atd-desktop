/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package nl.grootligt.klantenbinding;

import java.util.Date;
import nl.grootligt.parkeergarage.Auto;

/**
 * @author Rory
 */
public class AutoOnderhoud extends Herinnering {
    private Auto deAuto;

    public AutoOnderhoud(Auto deAuto) {
        this.deAuto = deAuto;
        datum = new Date();
        
        bericht = "Beste heer/mevrouw " + deAuto.getDeEigenaar().getAchternaam() + ", \n Uw auto " + deAuto.getMerk() + " heeft onderhoud nodig.";
    }

    public Auto getDeAuto() {
        return deAuto;
    }
}
