/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package nl.grootligt.klantenbinding;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import nl.grootligt.parkeergarage.Auto;

/**
 * @author Rory
 */
public class Klant {
    private String voornaam, achternaam, email;
    private Date geboortedatum, laatstGeweest;
    private boolean actief = true;
    
    private ArrayList<Auto> deAutos = new ArrayList<>();

    public Klant(String voornaam, String achternaam, String email, Date geboortedatum) {
        this.voornaam = voornaam;
        this.achternaam = achternaam;
        this.email = email;
        this.geboortedatum = geboortedatum;
        laatstGeweest = new Date();
    }
    
    public boolean addAuto(Auto a){
        if(a.getDeEigenaar()!=null)
            return false;
        for(Auto value : deAutos){
            if(value==a)
                return false;
        }
        deAutos.add(a);
        return true;
    }
    
    public boolean removeAuto(Auto a){
        if(a.getDeEigenaar()!=null || !a.isActief())
            return false;
        deAutos.remove(a);
        return true;
    }

    public String getVoornaam() {
        return voornaam;
    }

    public void setVoornaam(String voornaam) {
        this.voornaam = voornaam;
    }

    public String getAchternaam() {
        return achternaam;
    }

    public void setAchternaam(String achternaam) {
        this.achternaam = achternaam;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getGeboortedatum() {
        return geboortedatum;
    }

    public void setGeboortedatum(Date geboortedatum) {
        this.geboortedatum = geboortedatum;
    }

    public ArrayList<Auto> getDeAutos() {
        return deAutos;
    }

    public Date getLaatstGeweest() {
        return laatstGeweest;
    }

    public void setLaatstGeweest(Date laatstGeweest) {
        this.laatstGeweest = laatstGeweest;
    }

    public boolean isActief() {
        return actief;
    }

    public void setActief(boolean actief) {
        this.actief = actief;
    }
    
    public String getGeboorteDatumString() {
    	DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
    	String s = df.format(geboortedatum);
    	return s;
    }
    
    public int getAantalAutos() {
    	return deAutos.size();
    }
    public String toString() {
    	return voornaam + " " + achternaam;
    }
    
}
