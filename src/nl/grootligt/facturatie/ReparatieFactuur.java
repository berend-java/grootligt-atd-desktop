/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package nl.grootligt.facturatie;

import nl.grootligt.agenda.Reparatie;

import java.util.Date;

/**
 * @author Rory
 */
public class ReparatieFactuur extends Factuur{
    private final Reparatie deReparatie;
    
    private Date laatsteHerinnering;

    public ReparatieFactuur(Reparatie deReparatie) {
        this.deReparatie = deReparatie;
    }
    
    public boolean finish(){
        if(isGereed()){
            return false;
        }
        
        datum = new Date();
        laatsteHerinnering = new Date();
        return true;
    }

    public Reparatie getDeReparatie() {
        return deReparatie;
    }

    public Date getLaatsteHerinnering() {
        return laatsteHerinnering;
    }

    public void setLaatsteHerinnering(Date laatsteHerinnering) {
        this.laatsteHerinnering = laatsteHerinnering;
    }
    
   // public String toString() {
    //	return "Factuur voor Reparatie:" + deReparatie.getNaam();
    //}
}
