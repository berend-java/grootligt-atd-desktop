/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package nl.grootligt.facturatie;

import java.util.Date;
import nl.grootligt.voorraadbeheer.Brandstof;

/**
 * @author Rory
 */
public class TankFactuur extends Factuur {
    private final Brandstof deBrandstof;

    public TankFactuur(Brandstof deBrandstof) {
        this.deBrandstof = deBrandstof;
    }
    
    public boolean finish(){
        if(isGereed())
            return false;
        
        datum = new Date();
        
        return true;
    }

    public Brandstof getDeBrandstof() {
        return deBrandstof;
    }
    
    //public String toString() {
    //	return "Factuur voor Brandstof" + deBrandstof.getTSIC();
   // }
}
