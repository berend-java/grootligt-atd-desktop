/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package nl.grootligt.facturatie;

import java.util.ArrayList;
import java.util.Date;

/**
 * @author Rory
 */
public class Rapportage {
    private ArrayList<ParkeerFactuur> deParkFact = new ArrayList<>();
    private ArrayList<ReparatieFactuur> deRepFact = new ArrayList<>();
    private ArrayList<TankFactuur> deTankFact = new ArrayList<>();
    
    private Date datum;

    public Rapportage(ArrayList<ParkeerFactuur> deParkFact, ArrayList<ReparatieFactuur> deRepFact, ArrayList<TankFactuur> deTankFact) {
        datum = new Date();
        this.deParkFact = deParkFact;
        this.deRepFact = deRepFact;
        this.deTankFact = deTankFact;
    }
}
