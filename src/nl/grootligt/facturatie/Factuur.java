/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package nl.grootligt.facturatie;

import java.util.Date;

/**
 * @author Rory
 */
public abstract class Factuur {
    protected Date datum;
    protected boolean betaald = false, gereed = false;
    protected int bedrag;
    
    public abstract boolean finish();

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    public boolean isBetaald() {
        return betaald;
    }

    public void setBetaald(boolean betaald) {
        this.betaald = betaald;
    }

    public boolean isGereed() {
        return gereed;
    }

    public void setGereed(boolean gereed) {
        this.gereed = gereed;
    }
    
    public int getBedrag() {
    	return bedrag;
    }
}
