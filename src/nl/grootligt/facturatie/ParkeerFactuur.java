/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package nl.grootligt.facturatie;

import java.util.Date;

import nl.grootligt.parkeergarage.Reservering;

/**
 * @author Rory
 */
public class ParkeerFactuur extends Factuur{
    private final Reservering deReservering;

    public ParkeerFactuur(Reservering deReservering) {
        this.deReservering = deReservering;
    }

    public boolean finish(){
        if(isGereed())
            return false;
        datum = new Date();
        
        return true;
    }
    
    public Reservering getDeReservering() {
        return deReservering;
    }
    
   // public String toString() {
  //  	return "Factuur voor Plek:" + deReservering.getDePlek();
    //}
    
}
