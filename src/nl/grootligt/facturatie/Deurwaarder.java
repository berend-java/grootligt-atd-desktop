/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package nl.grootligt.facturatie;

import nl.grootligt.agenda.Reparatie;
import java.util.ArrayList;
import java.util.Date;
import nl.grootligt.parkeergarage.Reservering;
import nl.grootligt.voorraadbeheer.Brandstof;

/**
 * @author Rory
 */
public class Deurwaarder {
    private ArrayList<ParkeerFactuur> deParkFact = new ArrayList<>();
    private ArrayList<ReparatieFactuur> deRepFact = new ArrayList<>();
    private ArrayList<TankFactuur> deTankFact = new ArrayList<>();
    private ArrayList<Rapportage> deRapportages = new ArrayList<>();
    
    public Deurwaarder(){}
    
    public boolean addParkFact(Reservering r){
        for(ParkeerFactuur value : deParkFact){
            if(value.getDeReservering()==r)
                return false;
        }
        deParkFact.add(new ParkeerFactuur(r));
        return true;
    }
    
    public boolean addRepFact(Reparatie r){
        for(ReparatieFactuur value : deRepFact){
            if(value.getDeReparatie()==r)
                return false;
        }
        deRepFact.add(new ReparatieFactuur(r));
        return true;
    }
    
    public boolean addTankFact(Brandstof b){
        for(TankFactuur value : deTankFact){
            if(value.getDeBrandstof()==b)
                return false;
        }
        deTankFact.add(new TankFactuur(b));
        return true;
    }
    
    public ArrayList genereerHerinneringen(){
        ArrayList al = new ArrayList();
        
        Date d = new Date();
        d.setMonth(d.getMonth()-3);
        
        Date d2 = new Date();
        d.setMonth(d.getDay()-7);
        
        for(ReparatieFactuur value : deRepFact){
            if(!value.isBetaald() && value.getDatum().before(d) && value.getLaatsteHerinnering().before(d2))
                al.add(value);
        }
        
        return al;
    }
    
    public void sendHerinneringen(ArrayList<ReparatieFactuur> al){
        for(ReparatieFactuur value : al){
            sendBericht(value);
            value.setLaatsteHerinnering(new Date());
        }
    }

    private void sendBericht(ReparatieFactuur value) {
        System.out.println("bericht verstuurd naar " + value.getDeReparatie().getDeAuto().getDeEigenaar().getEmail());
    }
    
    public Rapportage genereerRapportage(Date date){
        ArrayList<ReparatieFactuur> al = (ArrayList<ReparatieFactuur>)deRepFact.clone();
        ArrayList<TankFactuur> al2 = (ArrayList<TankFactuur>)deTankFact.clone();
        ArrayList<ParkeerFactuur> al3 = (ArrayList<ParkeerFactuur>)deParkFact.clone();
        
        Date d = date;
        d.setDate(1);
        
        for(ReparatieFactuur value : al){
            if(value.getDatum().before(d))
                al.remove(value);
        }
        
        for(TankFactuur value : al2){
            if(value.getDatum().before(d))
                al2.remove(value);
        }
        
        for(ParkeerFactuur value : al3){
            if(value.getDatum().before(d))
                al3.remove(value);
        }
        
        return new Rapportage(al3, al, al2);
    }
    public boolean saveRapportage(Rapportage r){
        for(Rapportage value : deRapportages){
            if(value==r)
                return false;
        }
        deRapportages.add(r);
        return true;
    }
    
    public ArrayList<ParkeerFactuur> getParkeerFacturen() {
    	return deParkFact;
    }
    public ArrayList<ReparatieFactuur> getReparatieFacturen() {
    	return deRepFact;
    }
    public ArrayList<TankFactuur> getTankeFacturen() {
    	return deTankFact;
    }
}
