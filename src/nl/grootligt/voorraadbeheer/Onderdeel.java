/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package nl.grootligt.voorraadbeheer;

/**
 * @author Rory
 */
public class Onderdeel {
    private int artikelnummer, aantal;
    private double prijs;
    private String type;

    public Onderdeel(int artikelnummer, int aantal, double prijs, String type) {
        this.artikelnummer = artikelnummer;
        this.aantal = aantal;
        this.prijs = prijs;
        this.type = type;
    }
    public Onderdeel(){}

    public int getArtikelnummer() {
        return artikelnummer;
    }

    public void setArtikelnummer(int artikelnummer) {
        this.artikelnummer = artikelnummer;
    }

    public int getAantal() {
        return aantal;
    }

    public void setAantal(int aantal) {
        this.aantal = aantal;
    }

    public double getPrijs() {
        return prijs;
    }

    public void setPrijs(double prijs) {
        this.prijs = prijs;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
    public String toString() {
    	return type;
    }
}
