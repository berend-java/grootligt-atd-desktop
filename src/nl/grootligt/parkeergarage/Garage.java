/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package nl.grootligt.parkeergarage;

import java.util.ArrayList;
import java.util.Date;

/**
 * @author Rory
 */
public class Garage {
    private ArrayList<Auto> deAutos = new ArrayList<>();
    private ArrayList<Plek> dePlekken = new ArrayList<>();
    private ArrayList<Reservering> deReserveringen = new ArrayList<>();
    
    public Garage(){
        for(int i=1; i<21; i++)
            dePlekken.add(new Plek(i, 5.00));
    }
    
    public boolean plekReserveren(Auto a, Plek p, Date d){
        if(p.isBezet())
            return false;
        p.setBezet(true);
        deReserveringen.add(new Reservering(d, p, a));
        return true;
    }
    
    public boolean plekVrijmaken(Plek p, boolean s){
        if(!p.isBezet())
            return false;
        if(s)
            p.setSchoon(s);
        
        return getReservering(p).finish();
    }
    
    private Reservering getReservering(Plek p){
        ArrayList<Reservering> al = (ArrayList)deReserveringen.clone();
        Reservering r = new Reservering();
        
        for(Reservering value : al){
            if(value.getEinddatum()!=null && value.getEinddatum().before(new Date()))
                al.remove(value);
        }
        for(Reservering value : al){
            if(value.getDePlek()==p){
                r = value;
            }
        }
        return r;
    }
    
    public boolean addAuto(String t, String m, String b, String k){
        for(Auto value : deAutos){
            if(value.getKenteken().equals(k) && value.isActief()){
                System.out.println("Kenteken bestaat al!");
                return false;
            }
        }
        deAutos.add(new Auto(t, m, b, k));
        return true;
    }
    
    public boolean deleteAuto(Auto a){
        if(!checkAuto(a))
            return false;
        a.setActief(false);
        return false;
    }
    
    private boolean checkAuto(Auto a){
        for(Auto value : deAutos){
            if(value==a)
                return true;
        }
        return false;
    }
    
    public ArrayList<Auto> getAutosZonderEigenaar(){
        ArrayList<Auto> al = new ArrayList<>();
        
        for(Auto value : deAutos){
            if(value.getDeEigenaar()==null && value.isActief())
                al.add(value);
        }
        
        return al;
    }
    public ArrayList getRapportage(){
        ArrayList al = new ArrayList();
        
        
        return al;
    }
    
    public ArrayList<Auto> getAutos() {
    	return deAutos;
    }
    
    public ArrayList<Plek> getPlekken() {
    	return dePlekken;
    }
    
    public ArrayList<Reservering> getReserveringen() {
    	return deReserveringen;
    }
}
