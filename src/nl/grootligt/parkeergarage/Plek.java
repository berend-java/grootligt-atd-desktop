/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package nl.grootligt.parkeergarage;

/**
 * @author Rory
 */
public class Plek {
    private int pleknummer;
    private boolean bezet = false, schoon = true;
    private double prijs;

    public Plek(int pleknummer, double prijs) {
        this.pleknummer = pleknummer;
        this.prijs = prijs;
    }

    public int getPleknummer() {
        return pleknummer;
    }

    public void setPleknummer(int pleknummer) {
        this.pleknummer = pleknummer;
    }

    public boolean isBezet() {
        return bezet;
    }

    public void setBezet(boolean bezet) {
        this.bezet = bezet;
    }

    public boolean isSchoon() {
        return schoon;
    }

    public void setSchoon(boolean schoon) {
        this.schoon = schoon;
    }

    public double getPrijs() {
        return prijs;
    }

    public void setPrijs(double prijs) {
        this.prijs = prijs;
    }
    
    public String toString() {
    	return " " + pleknummer;
    }
}
