package nl.grootligt.gui;

import java.awt.*;
import java.awt.event.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import javax.swing.*;

import net.miginfocom.swing.MigLayout;
import nl.grootligt.klantenbinding.Klandizie;
import nl.grootligt.klantenbinding.Klant;
import nl.grootligt.utils.GLConfiguratie;

public class KlantAanpassenGui extends JFrame implements ActionListener {
	/**
	 * @author Berend de Groot
	 */
	private static final long serialVersionUID = 1L;
	private JButton aanpassen = new JButton("Aanpassen");
	private JLabel klantLabel = new JLabel("Kies Klant"), voornaamLabel = new JLabel("Voornaam"), achternaamLabel = new JLabel("Achternaam"), emailLabel = new JLabel("Email"), geboorteLabel = new JLabel("Geboorte Datum");
	private JTextField voornaamTf = new JTextField(25), achternaamTf = new JTextField(25), emailTf = new JTextField(25), geboorteTf = new JTextField(25);
	private Object[] cValues;
	private KlantenbindingGui kui;
	private JComboBox<?> klanten;

	public KlantAanpassenGui(Klandizie k, KlantenbindingGui kui) {
		this.kui = kui;
		Container p = getContentPane();
		MigLayout layout = new MigLayout();
		ArrayList<Klant> deKlanten = k.getDeKlanten();
		cValues = deKlanten.toArray();
		
		klanten = new JComboBox<Object>(cValues);
		klanten.addActionListener(this);
		
		setTitle("ATD -> Klantenbinding -> Herinnering Genereren");
		setSize(350, 230);
		setDefaultCloseOperation(HIDE_ON_CLOSE);
		setLayout(layout);
		setLocationRelativeTo(null);
		
		p.setBackground(GLConfiguratie.getFrameBackground());
		p.setForeground(Color.GREEN);
		klantLabel.setForeground(GLConfiguratie.getvButtonForeground());
		voornaamLabel.setForeground(GLConfiguratie.getvButtonForeground());
		achternaamLabel.setForeground(GLConfiguratie.getvButtonForeground());
		emailLabel.setForeground(GLConfiguratie.getvButtonForeground());
		geboorteLabel.setForeground(GLConfiguratie.getvButtonForeground());
		
		configureButton(aanpassen);
		
		add(klantLabel, "cell 0 0");
		add(klanten, "cell 1 0");
		add(voornaamLabel, "cell 0 1");
		add(voornaamTf, "cell 1 1");
		add(achternaamLabel, "cell 0 2 ");
		add(achternaamTf, "cell 1 2");
		add(emailLabel, "cell 0 3");
		add(emailTf, "cell 1 3");
		add(geboorteLabel, "cell 0 4");
		add(geboorteTf, "cell 1 4");
		add(aanpassen, "cell 0 5, gaptop 20");
		
		
        setVisible(true);  
	}
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == aanpassen) {
			System.out.println("-> " + aanpassen.getText());
			if(!voornaamTf.getText().equals("") && !achternaamTf.getText().equals("") && !emailTf.getText().equals("") && !geboorteTf.getText().equals("")) {
				Date date = null;
				try {
					date = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH).parse(geboorteTf.getText());
				} catch (ParseException e1) {
					JOptionPane.showMessageDialog(null, "Datum graag in de volgende format: dd-MM-yyyy");
					return;
				}
				Klant kk = (Klant) klanten.getSelectedItem();
				kk.setAchternaam(achternaamTf.getText());
				kk.setVoornaam(voornaamTf.getText());
				kk.setEmail(emailTf.getText());
				kk.setGeboortedatum(date);
				kui.refreshTable();
				
				JOptionPane.showMessageDialog(null, "Klant Aangepast");
				this.dispose();
			}
		}
		else if(e.getSource() == klanten) {
			Klant k = (Klant) klanten.getSelectedItem();
			voornaamTf.setText(k.getVoornaam());
			achternaamTf.setText(k.getAchternaam());
			emailTf.setText(k.getEmail());
			DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
			
			Date datum = k.getGeboortedatum();
			String sDatum = df.format(datum);
			
			geboorteTf.setText(sDatum);
		}
	}
	
	public void configureButton(JButton b) {
		b.setBackground(GLConfiguratie.getvButtonBackground());
		b.setForeground(GLConfiguratie.getvButtonForeground());
		b.setPreferredSize(new Dimension(100,30));
		b.addActionListener(this);
	}
}
