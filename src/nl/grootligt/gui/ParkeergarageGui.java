package nl.grootligt.gui;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

import javax.swing.*;

import net.miginfocom.swing.MigLayout;
import nl.grootligt.klantenbinding.Klandizie;
import nl.grootligt.parkeergarage.Garage;
import nl.grootligt.parkeergarage.Plek;
import nl.grootligt.parkeergarage.Reservering;
import nl.grootligt.utils.GLConfiguratie;
import nl.grootligt.utils.GLTableModelAgenda;
import nl.grootligt.utils.GLTableModelPlek;
import nl.grootligt.utils.GLTableModelReservering;
import nl.grootligt.voorraadbeheer.Magazijn;

public class ParkeergarageGui extends JFrame implements ActionListener {
	/**
	 * @author Berend de Groot
	 */
	private static final long serialVersionUID = 1L;
	private JButton plekReserveren, plekVrijmaken, autoOverzicht, autoAanmaken, autoAanpassen, mRapportageGenereren;
	private GLTableModelPlek GLT;
	private GLTableModelReservering GLT2;
	private Klandizie k;
	private Garage g;
	private Magazijn m;

	public ParkeergarageGui(Magazijn m, Klandizie k, Garage g) {
		this.k = k;
		this.g = g;
		this.m = m;
		Container p = getContentPane();
		MigLayout layout = new MigLayout();
		
		setTitle("Auto Totaal Diensten -> Agenda");
		setSize(800, 600);
		setDefaultCloseOperation(HIDE_ON_CLOSE);
		setLayout(layout);
		setLocationRelativeTo(null);
		setResizable(false);
		
		p.setBackground(GLConfiguratie.getFrameBackground());
		p.setForeground(GLConfiguratie.getFrameForeground());

		plekReserveren = new JButton("Plek Reserveren");
		plekVrijmaken = new JButton("Plek Vrijmaken");
		autoOverzicht = new JButton("Auto Overzicht");
		autoAanmaken = new JButton("Auto Aanmaken");
		autoAanpassen = new JButton("Auto Aanpassen");
		mRapportageGenereren = new JButton("Maandelijkse Rapportage Genereren");
		
		configureButton(plekReserveren);
		configureButton(plekVrijmaken);
		configureButton(autoOverzicht);
		configureButton(autoAanmaken);
		configureButton(autoAanpassen);
		configureButton(mRapportageGenereren);

		String[] columnNames = {"Plek", "Bezet", "Schoon", "Prijs"};
		ArrayList<Plek> data = g.getPlekken();
		
		String[] columnNames2 = {"Start Datum", "Auto", "Plek"};
		ArrayList<Reservering> data2 = g.getReserveringen();
					
		GLT = new GLTableModelPlek(columnNames, data);
		JTable table = new JTable(GLT);
		
		GLT2 = new GLTableModelReservering(columnNames2, data2);
		JTable table2 = new JTable(GLT2);
		
		table.getTableHeader().setBackground(GLConfiguratie.getvTableBackgrid());
		table.getTableHeader().setForeground(GLConfiguratie.getvButtonForeground());
		table.setBackground(GLConfiguratie.getFrameBackground());
		table.setForeground(GLConfiguratie.getvButtonForeground());
		table.setGridColor(GLConfiguratie.getvTableBackgrid());
		table.setSelectionBackground(GLConfiguratie.getvButtonBackground());
		table.setSelectionForeground(Color.WHITE);
		table.setMinimumSize(new Dimension(450, 80));
		
		table2.getTableHeader().setBackground(GLConfiguratie.getvTableBackgrid());
		table2.getTableHeader().setForeground(GLConfiguratie.getvButtonForeground());
		table2.setBackground(GLConfiguratie.getFrameBackground());
		table2.setForeground(GLConfiguratie.getvButtonForeground());
		table2.setGridColor(GLConfiguratie.getvTableBackgrid());
		table2.setSelectionBackground(GLConfiguratie.getvButtonBackground());
		table2.setSelectionForeground(Color.WHITE);
		table2.setMinimumSize(new Dimension(450, 100));

		JScrollPane sp = new JScrollPane(table);
		sp.setBorder(BorderFactory.createEmptyBorder());
		sp.getViewport().setBackground(GLConfiguratie.getFrameBackground());
		sp.getViewport().setBorder(null);
		sp.setMinimumSize(new Dimension(300, 100));
		
		JScrollPane sp2 = new JScrollPane(table2);
		sp2.setBorder(BorderFactory.createEmptyBorder());
		sp2.getViewport().setBackground(GLConfiguratie.getFrameBackground());
		sp2.getViewport().setBorder(null);
		sp2.setMinimumSize(new Dimension(435, 55));
		
		
		JComponent tables = new JPanel(layout);
		tables.setBackground(GLConfiguratie.getFrameBackground());
		tables.setMinimumSize(new Dimension(450, 100));
		JComponent comp = new JPanel(layout);
		
		tables.add(sp, "wrap, gaptop 5, gapleft 5");
		tables.add(sp2, "gapleft 5");
		comp.setBackground(GLConfiguratie.getFrameBackground());
		add(tables, "dock west, gapright 40");
		comp.add(plekReserveren, "wrap");
		comp.add(plekVrijmaken, "wrap");
		comp.add(autoOverzicht, "wrap");
		comp.add(autoAanmaken, "wrap");
		comp.add(autoAanpassen, "wrap");
		comp.add(mRapportageGenereren);
		add(comp, "dock east");

        setVisible(true);  
	}

	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == plekReserveren) {
			System.out.println("-> " + plekReserveren.getText());
			new PlekReserverenGui(k, g, this);
		}
		else if(e.getSource() == plekVrijmaken) {
			System.out.println("-> " + plekVrijmaken.getText());
			new PlekVrijmakenGui(g, this);
		}
		else if(e.getSource() == autoOverzicht) {
			System.out.println("-> " + autoOverzicht.getText());
			new AutoOverzichtGui(g);
		}
		else if(e.getSource() == autoAanmaken) {
			System.out.println("-> " + autoAanmaken.getText());
			new AutoAanmakenGui(m, g);
		}
		else if(e.getSource() == autoAanpassen) {
			System.out.println("-> " + autoAanpassen.getText());
			new AutoAanpassenGui(m, g);
		}
		else if(e.getSource() == mRapportageGenereren) {
			System.out.println("-> " + mRapportageGenereren.getText());
		}
	}
	
	public void configureButton(JButton b) {
		b.setBackground(GLConfiguratie.getvButtonBackground());
		b.setForeground(GLConfiguratie.getvButtonForeground());
		b.setPreferredSize(new Dimension(200,70));
		b.addActionListener(this);
	}
	
	public void refreshTable() {
		GLT.fireTableDataChanged();
		GLT2.fireTableDataChanged();
	}
}
