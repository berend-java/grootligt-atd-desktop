package nl.grootligt.gui;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import net.miginfocom.swing.MigLayout;
import nl.grootligt.utils.GLConfiguratie;
import nl.grootligt.voorraadbeheer.Magazijn;
import nl.grootligt.voorraadbeheer.Onderdeel;

public class OnderdeelBestellenGui extends JFrame implements ActionListener {
	/**
	 * @author Berend de Groot
	 */
	private static final long serialVersionUID = 1L;
	private Magazijn m;
	private JLabel onderdelen = new JLabel("Onderdeel"), aantal = new JLabel("Aantal"), vol = new JLabel("5 onderdelen per keer.");
	private JButton bestel = new JButton("Bestel"), plus = new JButton("+");
	private ArrayList<Onderdeel> deOnderdelen;
	private Object[] cValues;
	private JComboBox<?> verschillendeOnderdelen1, verschillendeOnderdelen2, verschillendeOnderdelen3, verschillendeOnderdelen4, verschillendeOnderdelen5;
	private JTextField onderdeel1 = new JTextField(10), onderdeel2 = new JTextField(10), onderdeel3 = new JTextField(10), onderdeel4 = new JTextField(10), onderdeel5 = new JTextField(10);
	private MigLayout layout = new MigLayout();
	private JPanel panel = new JPanel();
	private int aantalVelden = 1;
	private VoorraadBeheerGui j;
	
	
	public OnderdeelBestellenGui(Magazijn m, VoorraadBeheerGui j) {
		this.m = m;
		this.j = j;
		deOnderdelen = m.getDeOnderdelen();
		cValues = deOnderdelen.toArray();
		
		verschillendeOnderdelen1 = new JComboBox<Object>(cValues);
		verschillendeOnderdelen2 = new JComboBox<Object>(cValues);
		verschillendeOnderdelen3 = new JComboBox<Object>(cValues);
		verschillendeOnderdelen4 = new JComboBox<Object>(cValues);
		verschillendeOnderdelen5 = new JComboBox<Object>(cValues);
		
		Container p = getContentPane();
		
		setTitle("ATD -> Voorraad Beheer -> Onderdelen Bestellen");
		setSize(300, 300);
		setDefaultCloseOperation(HIDE_ON_CLOSE);
		setLayout(layout);
		setLocationRelativeTo(null);
		setResizable(false);
		
		p.setBackground(GLConfiguratie.getFrameBackground());
		p.setForeground(Color.GREEN);
		onderdelen.setForeground(GLConfiguratie.getvButtonForeground());
		aantal.setForeground(GLConfiguratie.getvButtonForeground());
		panel.setBackground(GLConfiguratie.getFrameBackground());
		panel.setMinimumSize(new Dimension(280, 200));
		vol.setForeground(Color.WHITE);
		
		configureButton(bestel);
		configureButton(plus);
		plus.setPreferredSize(new Dimension(30,20));
		
		add(onderdelen, "gapright 60, gapleft 23");
		add(aantal, "wrap");
		panel.add(verschillendeOnderdelen1);
		panel.add(onderdeel1);
		add(panel, "span");
		add(plus);
		add(bestel);
		
		setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == plus) {
			System.out.println("-> " + plus.getText());
			switch(aantalVelden) {
				case 1: panel.add(verschillendeOnderdelen2); panel.add(onderdeel2); break;
				case 2: panel.add(verschillendeOnderdelen3); panel.add(onderdeel3); break;
				case 3: panel.add(verschillendeOnderdelen4); panel.add(onderdeel4); break;
				case 4: panel.add(verschillendeOnderdelen5); panel.add(onderdeel5); plus.setEnabled(false); break;
				case 5: panel.add(vol); break;
			}
			aantalVelden++;
			panel.validate();
			panel.repaint();
			
		} else if(e.getSource() == bestel) {
			System.out.println("-> " + bestel.getText());
			boolean b1 = false;
			
			if(!verschillendeOnderdelen1.getSelectedItem().equals("") && !onderdeel1.getText().equals("")) {
				Onderdeel o = (Onderdeel) verschillendeOnderdelen1.getSelectedItem();
				int a = Integer.parseInt(onderdeel1.getText());
				if(a < 0) {
					JOptionPane.showMessageDialog(null, "Het bedrag bij het eerste onderdeel moet hoger als 0 zijn.");
					return;
				}
				b1 = m.addOnderdeel(o.getArtikelnummer(), a, 0, o.getType());
			}
			if(!verschillendeOnderdelen2.getSelectedItem().equals("") && !onderdeel2.getText().equals("")) {
				Onderdeel o = (Onderdeel) verschillendeOnderdelen2.getSelectedItem();
				int a = Integer.parseInt(onderdeel2.getText());
				if(a < 0) {
					JOptionPane.showMessageDialog(null, "Het bedrag bij het tweede onderdeel moet hoger als 0 zijn.");
					return;
				}
				m.addOnderdeel(o.getArtikelnummer(), a, 0, o.getType());
			}
			if(!verschillendeOnderdelen3.getSelectedItem().equals("") && !onderdeel3.getText().equals("")) {
				Onderdeel o = (Onderdeel) verschillendeOnderdelen3.getSelectedItem();
				int a = Integer.parseInt(onderdeel3.getText());
				if(a < 0) {
					JOptionPane.showMessageDialog(null, "Het bedrag bij het derde onderdeel moet hoger als 0 zijn.");
					return;
				}
				m.addOnderdeel(o.getArtikelnummer(), a, 0, o.getType());
			}
			if(!verschillendeOnderdelen4.getSelectedItem().equals("") && !onderdeel4.getText().equals("")) {
				Onderdeel o = (Onderdeel) verschillendeOnderdelen4.getSelectedItem();
				int a = Integer.parseInt(onderdeel4.getText());
				if(a < 0) {
					JOptionPane.showMessageDialog(null, "Het bedrag bij het vierde onderdeel moet hoger als 0 zijn.");
					return;
				}
				m.addOnderdeel(o.getArtikelnummer(), a, 0, o.getType());
			}
			if(!verschillendeOnderdelen5.getSelectedItem().equals("") && !onderdeel5.getText().equals("")) {
				Onderdeel o = (Onderdeel) verschillendeOnderdelen5.getSelectedItem();
				int a = Integer.parseInt(onderdeel5.getText());
				if(a < 0) {
					JOptionPane.showMessageDialog(null, "Het bedrag bij het vijfde onderdeel moet hoger als 0 zijn.");
					return;
				}
				m.addOnderdeel(o.getArtikelnummer(), a, 0, o.getType());
			}
			
			if(b1) {
				JOptionPane.showMessageDialog(null, "Onderdeel(en) Besteld");
				this.dispose();
			} else {
				JOptionPane.showMessageDialog(null, "Onderdeel(en) niet Besteld");
			}
			
			j.refreshTable();
		}	
	}
	
	public void configureButton(JButton b) {
		b.setBackground(GLConfiguratie.getvButtonBackground());
		b.setForeground(GLConfiguratie.getvButtonForeground());
		b.setPreferredSize(new Dimension(100,30));
		b.addActionListener(this);
	}
}
