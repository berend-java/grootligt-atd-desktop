package nl.grootligt.gui;

import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import net.miginfocom.swing.MigLayout;
import nl.grootligt.agenda.Agenda;
import nl.grootligt.agenda.Monteur;
import nl.grootligt.agenda.Reparatie;
import nl.grootligt.utils.GLConfiguratie;

public class MonteurKoppelenGui extends JFrame implements ActionListener {
	/**
	 * @author Berend de Groot
	 */
	private static final long serialVersionUID = 1L;
	private JLabel reparatie = new JLabel("Kies Reparatie"), monteur = new JLabel("Kies Monteur");
	private JButton koppel = new JButton("Koppel");
	private Object[] cValues, cValues2;
	private JComboBox<?> monteurs, reparaties;
	private MigLayout layout = new MigLayout();
	private Agenda a;
	private AgendaGui agui;
	
	public MonteurKoppelenGui(Agenda a, AgendaGui agui) {
		this.a = a;
		this.agui = agui;

		cValues = a.getReparaties().toArray();
		cValues2 = a.getMonteurs().toArray();
		
		reparaties = new JComboBox<Object>(cValues);
		monteurs = new JComboBox<Object>(cValues2);
		
		Container p = getContentPane();
		
		setTitle("ATD -> Voorraad Beheer -> Koppel Auto");
		setSize(250, 220);
		setDefaultCloseOperation(HIDE_ON_CLOSE);
		setLayout(layout);
		setLocationRelativeTo(null);
		setResizable(false);
		
		p.setBackground(GLConfiguratie.getFrameBackground());
		p.setForeground(Color.GREEN);
		monteur.setForeground(GLConfiguratie.getvButtonForeground());
		reparatie.setForeground(GLConfiguratie.getvButtonForeground());
		
		configureButton(koppel);
		
		add(monteur);
		add(monteurs, "wrap 40, gaptop 20");
		add(reparatie);
		add(reparaties, "wrap 40");
		add(koppel, "dock south");
		
		setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == koppel) {
			System.out.println("-> " + koppel.getText());
			Reparatie dReparatie = (Reparatie) reparaties.getSelectedItem();
			Monteur dMonteur = (Monteur) monteurs.getSelectedItem();
			boolean b = a.koppelMonRep(dMonteur, dReparatie);
				
			if(b) {
				JOptionPane.showMessageDialog(null, "Monteur en Reparatie Gekoppeld");
				this.dispose();
			} else {
				JOptionPane.showMessageDialog(null, "Monteur en Reparatie niet Gekoppeld");
			}
			agui.refreshTable();
		}
	}
	
	public void configureButton(JButton b) {
		b.setBackground(GLConfiguratie.getvButtonBackground());
		b.setForeground(GLConfiguratie.getvButtonForeground());
		b.addActionListener(this);
	}
}
