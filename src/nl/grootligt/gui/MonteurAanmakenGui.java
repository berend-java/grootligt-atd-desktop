package nl.grootligt.gui;

import java.awt.*;
import java.awt.event.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.*;

import net.miginfocom.swing.MigLayout;
import nl.grootligt.agenda.Agenda;
import nl.grootligt.utils.GLConfiguratie;

public class MonteurAanmakenGui extends JFrame implements ActionListener {
	/**
	 * @author Berend de Groot
	 */
	private static final long serialVersionUID = 1L;
	private JButton Aanmaken = new JButton("Aanmaken");
	private JLabel voornaamLabel = new JLabel("Voornaam"), achternaamLabel = new JLabel("Achternaam"), datumlabel = new JLabel("Geboorte Datum");
	private JTextField voornaamTf = new JTextField(15), achternaamTf = new JTextField(15), datumTf = new JTextField(15);
	private Agenda a;

	public MonteurAanmakenGui(Agenda a, AgendaGui aui) {
		this.a = a;
		Container p = getContentPane();
		MigLayout layout = new MigLayout();
		
		setTitle("ATD -> Klantenbinding -> Monteur Aanmaken");
		setSize(350, 230);
		setDefaultCloseOperation(HIDE_ON_CLOSE);
		setLayout(layout);
		setLocationRelativeTo(null);
		
		p.setBackground(GLConfiguratie.getFrameBackground());
		p.setForeground(Color.GREEN);
		achternaamLabel.setForeground(GLConfiguratie.getvButtonForeground());
		voornaamLabel.setForeground(GLConfiguratie.getvButtonForeground());
		datumlabel.setForeground(GLConfiguratie.getvButtonForeground());
		
		configureButton(Aanmaken);
		
		add(voornaamLabel, "cell 0 0");
		add(voornaamTf, "cell 1 0");
		add(achternaamLabel, "cell 0 1");
		add(achternaamTf, "cell 1 1");
		add(datumlabel, " cell 0 2");
		add(datumTf, "cell 1 2");
		add(Aanmaken, "cell 0 5, gaptop 20");
		
        setVisible(true);  
	}
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == Aanmaken) {
			System.out.println("-> " + Aanmaken.getText());
			if(!voornaamTf.getText().equals("") && !achternaamTf.getText().equals("") && !datumTf.getText().equals("")) {
				Date date = null;
				try {
					date = new SimpleDateFormat("dd-MM-yyyy").parse(datumTf.getText());
				} catch (ParseException e1) {
					JOptionPane.showMessageDialog(null, "Datum graag in de volgende format: dd-MM-yyyy");
					return;
				}
				a.addMonteur(voornaamTf.getText(), achternaamTf.getText(), date);
				JOptionPane.showMessageDialog(null, "Monteur toegevoegd");
				this.dispose();
			}
		}
	}
	
	public void configureButton(JButton b) {
		b.setBackground(GLConfiguratie.getvButtonBackground());
		b.setForeground(GLConfiguratie.getvButtonForeground());
		b.setPreferredSize(new Dimension(100,30));
		b.addActionListener(this);
	}
}
