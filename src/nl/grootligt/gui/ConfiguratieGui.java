package nl.grootligt.gui;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import net.miginfocom.swing.MigLayout;
import nl.grootligt.utils.GLConfiguratie;

public class ConfiguratieGui extends JFrame implements ActionListener {
	/**
	 * @author Berend de Groot
	 */
	private static final long serialVersionUID = 1L;
	private JLabel frameBackgroundLabel = new JLabel("Frame background:");
	private JComboBox<?> frameBackgroundBox;
	private JButton toepassen = new JButton("Toepassen");
	private Container p = getContentPane();
	private ATDGui atdGui;
	
	ConfiguratieGui(ATDGui atdGui) {
		this.atdGui = atdGui;
		MigLayout layout = new MigLayout();
		
		setTitle("Auto Totaal Diensten -> Configuratie");
		setSize(700, 420);
		setDefaultCloseOperation(HIDE_ON_CLOSE);
		setLayout(layout);
		setLocationRelativeTo(null);
		setResizable(false);
		
		p.setBackground(GLConfiguratie.getFrameBackground());
		frameBackgroundLabel.setForeground(GLConfiguratie.getvButtonForeground());

		String[] cValues = {"Default", "Black", "Gray"};
		frameBackgroundBox = new JComboBox<Object>(cValues);
		configureButton(toepassen);
		
		add(frameBackgroundLabel);
		add(frameBackgroundBox);
		add(toepassen, "dock south");
		
        setVisible(true);  
	}
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == toepassen) {
			Color color = null;
			switch(frameBackgroundBox.getSelectedItem().toString()) {
				case "Black":
					color = Color.BLACK;
				break;
				case "Gray":
					color = Color.GRAY;
				break;
				case "Default":
					color = new Color(71, 71, 71);
				break;
			}
			GLConfiguratie.setFrameBackground(color);
			atdGui.setFrameBackground(color);
			p.setBackground(color);
			repaint();
			
		}
		
	}
	
	
	public void configureButton(JButton b) {
		b.setBackground(GLConfiguratie.getvButtonBackground());
		b.setForeground(GLConfiguratie.getvButtonForeground());
		b.setPreferredSize(new Dimension(100,25));
		b.addActionListener(this);
	}
}

