package nl.grootligt.gui;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import net.miginfocom.swing.MigLayout;
import nl.grootligt.agenda.Agenda;
import nl.grootligt.agenda.Reparatie;
import nl.grootligt.utils.GLConfiguratie;
import nl.grootligt.voorraadbeheer.Magazijn;
import nl.grootligt.voorraadbeheer.Onderdeel;

public class GoederenToevoegenReparatieGui extends JFrame implements ActionListener {
	/**
	 * @author Berend de Groot
	 */
	private static final long serialVersionUID = 1L;
	private JLabel onderdelen = new JLabel("Onderdeel"), aantal = new JLabel("Aantal"), vol = new JLabel("5 onderdelen per keer."), reparatieLabel = new JLabel("Selecteer Reparatie");
	private JButton bestel = new JButton("Voeg toe"), plus = new JButton("+");
	private ArrayList<Onderdeel> deOnderdelen;
	private Object[] cValues, cValues2;
	private JComboBox<?> verschillendeOnderdelen1, verschillendeOnderdelen2, verschillendeOnderdelen3, verschillendeOnderdelen4, verschillendeOnderdelen5, reparaties;
	private JTextField onderdeel1 = new JTextField(10), onderdeel2 = new JTextField(10), onderdeel3 = new JTextField(10), onderdeel4 = new JTextField(10), onderdeel5 = new JTextField(10);
	private MigLayout layout = new MigLayout();
	private JPanel panel = new JPanel();
	private int aantalVelden = 1;
	private AgendaGui aui;
	
	public GoederenToevoegenReparatieGui(Magazijn m, Agenda a, AgendaGui aui) {
		this.aui = aui;
		
		deOnderdelen = m.getDeOnderdelen();
		cValues = deOnderdelen.toArray();
		cValues2 = a.getReparaties().toArray();
		
		reparaties = new JComboBox<Object>(cValues2);
		verschillendeOnderdelen1 = new JComboBox<Object>(cValues);
		verschillendeOnderdelen2 = new JComboBox<Object>(cValues);
		verschillendeOnderdelen3 = new JComboBox<Object>(cValues);
		verschillendeOnderdelen4 = new JComboBox<Object>(cValues);
		verschillendeOnderdelen5 = new JComboBox<Object>(cValues);
		
		Container p = getContentPane();
		
		setTitle("ATD -> Voorraad Beheer -> Onderdelen Bestellen");
		setSize(300, 340);
		setDefaultCloseOperation(HIDE_ON_CLOSE);
		setLayout(layout);
		setLocationRelativeTo(null);
		setResizable(false);
		
		p.setBackground(GLConfiguratie.getFrameBackground());
		p.setForeground(Color.GREEN);
		reparatieLabel.setForeground(GLConfiguratie.getvButtonForeground());
		onderdelen.setForeground(GLConfiguratie.getvButtonForeground());
		aantal.setForeground(GLConfiguratie.getvButtonForeground());
		panel.setBackground(GLConfiguratie.getFrameBackground());
		panel.setMinimumSize(new Dimension(280, 200));
		vol.setForeground(Color.WHITE);
		
		configureButton(bestel);
		configureButton(plus);
		plus.setPreferredSize(new Dimension(30,20));
		
		add(reparatieLabel);
		add(reparaties, "wrap");
		add(onderdelen, "gapright 60, gapleft 23");
		add(aantal, "wrap");
		panel.add(verschillendeOnderdelen1);
		panel.add(onderdeel1);
		add(panel, "span");
		add(plus);
		add(bestel);
		
		setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == plus) {
			System.out.println("-> " + plus.getText());
			switch(aantalVelden) {
				case 1: panel.add(verschillendeOnderdelen2); panel.add(onderdeel2); break;
				case 2: panel.add(verschillendeOnderdelen3); panel.add(onderdeel3); break;
				case 3: panel.add(verschillendeOnderdelen4); panel.add(onderdeel4); break;
				case 4: panel.add(verschillendeOnderdelen5); panel.add(onderdeel5); plus.setEnabled(false); break;
				case 5: panel.add(vol); break;
			}
			aantalVelden++;
			panel.validate();
			panel.repaint();
			
		} else if(e.getSource() == bestel) {
			System.out.println("-> " + bestel.getText());
			Reparatie r = (Reparatie) reparaties.getSelectedItem();
			if(!verschillendeOnderdelen1.getSelectedItem().equals("") && !onderdeel1.getText().equals("")) {
				Onderdeel o = (Onderdeel) verschillendeOnderdelen1.getSelectedItem();
				int a = Integer.parseInt(onderdeel1.getText());
				if(a < 0) {
					JOptionPane.showMessageDialog(null, "Het bedrag bij het eerste onderdeel moet hoger als 0 zijn.");
					return;
				}
				o.setAantal(o.getAantal()-a);
				r.addOnderdeel(o);
				
			}
			if(!verschillendeOnderdelen2.getSelectedItem().equals("") && !onderdeel2.getText().equals("")) {
				Onderdeel o = (Onderdeel) verschillendeOnderdelen2.getSelectedItem();
				int a = Integer.parseInt(onderdeel2.getText());
				if(a < 0) {
					JOptionPane.showMessageDialog(null, "Het bedrag bij het tweede onderdeel moet hoger als 0 zijn.");
					return;
				}
				o.setAantal(o.getAantal()-a);
				r.addOnderdeel(o);
			}
			if(!verschillendeOnderdelen3.getSelectedItem().equals("") && !onderdeel3.getText().equals("")) {
				Onderdeel o = (Onderdeel) verschillendeOnderdelen3.getSelectedItem();
				int a = Integer.parseInt(onderdeel3.getText());
				if(a < 0) {
					JOptionPane.showMessageDialog(null, "Het bedrag bij het derde onderdeel moet hoger als 0 zijn.");
					return;
				}
				o.setAantal(o.getAantal()-a);
				r.addOnderdeel(o);
			}
			if(!verschillendeOnderdelen4.getSelectedItem().equals("") && !onderdeel4.getText().equals("")) {
				Onderdeel o = (Onderdeel) verschillendeOnderdelen4.getSelectedItem();
				int a = Integer.parseInt(onderdeel4.getText());
				if(a < 0) {
					JOptionPane.showMessageDialog(null, "Het bedrag bij het vierde onderdeel moet hoger als 0 zijn.");
					return;
				}
				o.setAantal(o.getAantal()-a);
				r.addOnderdeel(o);
			}
			if(!verschillendeOnderdelen5.getSelectedItem().equals("") && !onderdeel5.getText().equals("")) {
				Onderdeel o = (Onderdeel) verschillendeOnderdelen5.getSelectedItem();
				int a = Integer.parseInt(onderdeel5.getText());
				if(a < 0) {
					JOptionPane.showMessageDialog(null, "Het bedrag bij het vijfde onderdeel moet hoger als 0 zijn.");
					return;
				}
				o.setAantal(o.getAantal()-a);
				r.addOnderdeel(o);
			}

			JOptionPane.showMessageDialog(null, "Onderdeel(en) toegevoegd, en verwijderd uit magazijn");
			this.dispose();

			aui.refreshTable();
		}	
	}
	
	public void configureButton(JButton b) {
		b.setBackground(GLConfiguratie.getvButtonBackground());
		b.setForeground(GLConfiguratie.getvButtonForeground());
		b.setPreferredSize(new Dimension(100,30));
		b.addActionListener(this);
	}
}
