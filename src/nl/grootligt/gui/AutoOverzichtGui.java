package nl.grootligt.gui;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

import javax.swing.*;

import net.miginfocom.swing.MigLayout;
import nl.grootligt.klantenbinding.Klandizie;
import nl.grootligt.klantenbinding.Klant;
import nl.grootligt.parkeergarage.Auto;
import nl.grootligt.parkeergarage.Garage;
import nl.grootligt.utils.GLConfiguratie;
import nl.grootligt.utils.GLTableModelAuto;
import nl.grootligt.utils.GLTableModelKlant;

public class AutoOverzichtGui extends JFrame {
	/**
	 * @author Berend de Groot
	 */
	private static final long serialVersionUID = 1L;
	private Garage g;
	private GLTableModelAuto GLTable;

	public AutoOverzichtGui(Garage g) {
		this.g = g;
		Container p = getContentPane();
		MigLayout layout = new MigLayout();
		
		setTitle("Auto Totaal Diensten -> Auto Overzicht");
		setSize(750, 420);
		setDefaultCloseOperation(HIDE_ON_CLOSE);
		setLayout(layout);
		setLocationRelativeTo(null);
		setResizable(false);
		
		p.setBackground(GLConfiguratie.getFrameBackground());



		String[] columnNames = {"Kenteken", "Type", "Merk", "Eigenaar", "Brandstof"};
		ArrayList<Auto> data = g.getAutos();
		GLTable = new GLTableModelAuto(columnNames, data);
		JTable table = new JTable(GLTable);

		table.setMinimumSize(new Dimension(700, 550));
		table.getTableHeader().setBackground(GLConfiguratie.getvTableBackgrid());
		table.getTableHeader().setForeground(GLConfiguratie.getvButtonForeground());
		table.setBackground(GLConfiguratie.getFrameBackground());
		table.setForeground(GLConfiguratie.getvButtonForeground());
		table.setGridColor(GLConfiguratie.getvTableBackgrid());
		table.setSelectionBackground(GLConfiguratie.getvButtonBackground());
		table.setSelectionForeground(Color.WHITE);

		JScrollPane sp = new JScrollPane(table);
		
		sp.setBorder(BorderFactory.createEmptyBorder());
		sp.getViewport().setBackground(GLConfiguratie.getFrameBackground());
		sp.getViewport().setBorder(null);
		sp.setMinimumSize(new Dimension(700, 550));


		add(sp, "dock west, gapright 60, gaptop 8, gapleft 8");
        setVisible(true);  
	}
}
