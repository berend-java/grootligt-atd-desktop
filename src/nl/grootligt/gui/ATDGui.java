package nl.grootligt.gui;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import net.miginfocom.swing.MigLayout;
import nl.grootligt.agenda.Agenda;
import nl.grootligt.facturatie.Deurwaarder;
import nl.grootligt.klantenbinding.Klandizie;
import nl.grootligt.parkeergarage.Garage;
import nl.grootligt.utils.GLConfiguratie;
import nl.grootligt.utils.GLTestData;
import nl.grootligt.voorraadbeheer.Magazijn;

public class ATDGui extends JFrame implements ActionListener {
	/**
	 * @author Berend de Groot
	 */
	private static final long serialVersionUID = 1L;
	private JLabel welcome = new JLabel("ATD Informatie systeem versie 1.1 door Berend de Groot & Rory van Ligten");
	private JButton agendaButton, facturatieButton, garageButton, voorraadButton, klantenButton, configuratieButton;
	private Magazijn m = new Magazijn();
	private Klandizie k = new Klandizie();
	private Garage g = new Garage();
	private Container p = getContentPane();
	private Agenda a = new Agenda();
	private Deurwaarder d = new Deurwaarder();
	
	public ATDGui() {
		MigLayout layout = new MigLayout();
		GLTestData GLT = new GLTestData(m, k, g, a, d);
		GLT.pushKlanten();
		GLT.pushOnderdelen();
		GLT.pushAutos();
		GLT.pushReparaties();
		GLT.pushFacturen();
		GLT.pushMonteurs();
		
		setTitle("Auto Totaal Diensten");
		setSize(800, 500);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLayout(layout);
		setLocationRelativeTo(null);
		setResizable(false);
		
		p.setBackground(GLConfiguratie.getFrameBackground());
		welcome.setForeground(Color.WHITE);
		welcome.setFont(welcome.getFont().deriveFont(9.0f));
		
		agendaButton = new JButton("Agenda");
		facturatieButton = new JButton("Facturatie");
		garageButton = new JButton("ParkeerGarage");
		voorraadButton = new JButton("Voorraad Beheer");
		klantenButton = new JButton("Klantenbinding");
		configuratieButton = new JButton("Configuratie");
		
		configureButton(agendaButton);
		configureButton(facturatieButton);
		configureButton(garageButton);
		configureButton(voorraadButton);
		configureButton(klantenButton);
		configureButton(configuratieButton);
			
		add(klantenButton, "cell 0 0");
		add(voorraadButton, "cell 1 0");
		add(agendaButton, "cell 0 1");
		add(garageButton, "cell 1 1");
		add(facturatieButton, "cell 0 2");
		add(configuratieButton, "cell 1 2");
		add(welcome, "cell 0 3, span 2 0");
		
        setVisible(true);  
        pack();
	}

	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == configuratieButton) {
			System.out.println("-> " + configuratieButton.getText());
			new ConfiguratieGui(this);
		}
		else if(e.getSource() == agendaButton) {
			System.out.println("-> " + agendaButton.getText());
			new AgendaGui(a, k, g, m);
		}
		else if(e.getSource() == facturatieButton) {
			System.out.println("-> " + facturatieButton.getText());
			new FacturatieGui(d);
		}
		else if(e.getSource() == garageButton) {
			System.out.println("-> " + garageButton.getText());
			new ParkeergarageGui(m, k, g);
		}
		else if(e.getSource() == voorraadButton) {
			System.out.println("-> " + voorraadButton.getText());
			new VoorraadBeheerGui(m);
		}
		else if(e.getSource() == klantenButton) {
			System.out.println("-> " + klantenButton.getText());
			new KlantenbindingGui(k, g);
		}
	}
	
	public void setFrameBackground(Color color) {
		p.setBackground(color);
	}
	
	public void configureButton(JButton b) {
		b.setBackground(GLConfiguratie.getFrameForeground());
		b.setForeground(GLConfiguratie.getButtonForegound());
		b.setPreferredSize(new Dimension(250,70));
		b.addActionListener(this);
	}
	

	public static void main(String[] args)  {		
		new ATDGui();
	}
}