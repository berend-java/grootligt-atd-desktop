package nl.grootligt.gui;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.*;

import net.miginfocom.swing.MigLayout;
import nl.grootligt.facturatie.Deurwaarder;
import nl.grootligt.facturatie.ParkeerFactuur;
import nl.grootligt.facturatie.ReparatieFactuur;
import nl.grootligt.facturatie.TankFactuur;
import nl.grootligt.utils.GLConfiguratie;
import nl.grootligt.utils.GLTableModelParkeerFactuur;
import nl.grootligt.utils.GLTableModelReparatieFactuur;
import nl.grootligt.utils.GLTableModelTankFactuur;

public class FacturatieGui extends JFrame implements ActionListener {
	/**
	 * @author Berend de Groot
	 */
	private static final long serialVersionUID = 1L;
	private JButton FactuurGenereren, RapportageGenereren, FactuurAanpassen, HerinneringSturen;
	private GLTableModelParkeerFactuur GLTp;
	private GLTableModelReparatieFactuur GLTr;
	private GLTableModelTankFactuur GLTt;
	private Deurwaarder dd;
	
	public FacturatieGui(Deurwaarder d) {
		this.dd = d;
		Container p = getContentPane();
		MigLayout layout = new MigLayout();
		
		setTitle("Auto Totaal Diensten -> Facturatie");
		setSize(800, 400);
		setDefaultCloseOperation(HIDE_ON_CLOSE);
		setLayout(layout);
		setLocationRelativeTo(null);
		setResizable(false);
		
		p.setBackground(GLConfiguratie.getFrameBackground());
		p.setForeground(GLConfiguratie.getFrameForeground());

		FactuurGenereren = new JButton("Factuur Genereren");
		RapportageGenereren = new JButton("Rapportage Genereren");
		FactuurAanpassen = new JButton("Factuur Aanpassen");
		HerinneringSturen = new JButton("Herinnering Sturen");
		
		configureButton(FactuurGenereren);
		configureButton(RapportageGenereren);
		configureButton(FactuurAanpassen);
		configureButton(HerinneringSturen);


		String[] columnNames = {"Datum", "Bedrag", "isBetaald", "isGereed"};
		
		ArrayList<ParkeerFactuur> data = d.getParkeerFacturen();
		ArrayList<ReparatieFactuur> data2 = d.getReparatieFacturen();
		ArrayList<TankFactuur> data3 = d.getTankeFacturen();

		GLTp = new GLTableModelParkeerFactuur(columnNames, data);
		GLTr = new GLTableModelReparatieFactuur(columnNames, data2);
		GLTt = new GLTableModelTankFactuur(columnNames, data3);
		
		JTable table = new JTable(GLTp);
		JTable table2 = new JTable(GLTr);
		JTable table3 = new JTable(GLTt);
		
		table.getTableHeader().setBackground(GLConfiguratie.getvTableBackgrid());
		table.getTableHeader().setForeground(GLConfiguratie.getvButtonForeground());
		table.setBackground(GLConfiguratie.getFrameBackground());
		table.setForeground(GLConfiguratie.getvButtonForeground());
		table.setGridColor(GLConfiguratie.getvTableBackgrid());
		table.setMinimumSize(new Dimension(450, 50));
		table.setSelectionBackground(GLConfiguratie.getvButtonBackground());
		table.setSelectionForeground(Color.WHITE);
		
		/* table2.getTableHeader().setBackground(GLConfiguratie.getvTableBackgrid());
		table2.getTableHeader().setForeground(GLConfiguratie.getvButtonForeground());
		table2.setBackground(GLConfiguratie.getFrameBackground());
		table2.setForeground(GLConfiguratie.getvButtonForeground());
		table2.setGridColor(GLConfiguratie.getvTableBackgrid());
		table2.setMinimumSize(new Dimension(450, 50));
		table2.setSelectionBackground(GLConfiguratie.getvButtonBackground());
		table2.setSelectionForeground(Color.WHITE);
		
		table3.getTableHeader().setBackground(GLConfiguratie.getvTableBackgrid());
		table3.getTableHeader().setForeground(GLConfiguratie.getvButtonForeground());
		table3.setBackground(GLConfiguratie.getFrameBackground());
		table3.setForeground(GLConfiguratie.getvButtonForeground());
		table3.setGridColor(GLConfiguratie.getvTableBackgrid());
		table3.setMinimumSize(new Dimension(450, 50));
		table3.setSelectionBackground(GLConfiguratie.getvButtonBackground());
		table3.setSelectionForeground(Color.WHITE); */
		

		JScrollPane sp = new JScrollPane(table);
		sp.setBorder(BorderFactory.createEmptyBorder());
		sp.getViewport().setBackground(GLConfiguratie.getFrameBackground());
		sp.getViewport().setBorder(null);
		sp.setMinimumSize(new Dimension(420, 55));
		
		/* JScrollPane sp2 = new JScrollPane(table2);
		sp2.setBorder(BorderFactory.createEmptyBorder());
		sp2.getViewport().setBackground(GLConfiguratie.getFrameBackground());
		sp2.getViewport().setBorder(null);
		sp2.setMinimumSize(new Dimension(435, 55));
		sp2.setMaximumSize(new Dimension(435, 55));
		
		JScrollPane sp3 = new JScrollPane(table3);
		sp3.setBorder(BorderFactory.createEmptyBorder());
		sp3.getViewport().setBackground(GLConfiguratie.getFrameBackground());
		sp3.getViewport().setBorder(null);
		sp3.setMinimumSize(new Dimension(435, 55));
		sp3.setMaximumSize(new Dimension(435, 55)); */
		
		JComponent tables = new JPanel(layout);
		tables.setBackground(GLConfiguratie.getFrameBackground());
		tables.setMinimumSize(new Dimension(450, 100));
		
		JComponent comp = new JPanel(layout);
		comp.setBackground(GLConfiguratie.getFrameBackground());
		
		tables.add(sp, "wrap");
		//tables.add(sp2, "wrap");
		//tables.add(sp3);
		add(tables, "dock west, gapright 130");
		comp.add(FactuurGenereren, "wrap");
		comp.add(RapportageGenereren, "wrap");
		comp.add(FactuurAanpassen, "wrap");
		comp.add(HerinneringSturen);
		add(comp, "dock east");

        setVisible(true);  
	}

	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == FactuurGenereren) {
			System.out.println("-> " + FactuurGenereren.getText());
			JOptionPane.showMessageDialog(null, "Facturen zijn Gegenereerd");
		}
		else if(e.getSource() == RapportageGenereren) {
			System.out.println("-> " + RapportageGenereren.getText());
			//dd.genereerRapportage(new Date());
			JOptionPane.showMessageDialog(null, "Rapportage is Gegenereerd");
		}
		else if(e.getSource() == FactuurAanpassen) {
			System.out.println("-> " + FactuurAanpassen.getText());
			new FactuurAanpassenGui(this, dd);
		}
		else if(e.getSource() == HerinneringSturen) {
			System.out.println("-> " + HerinneringSturen.getText());
			//dd.genereerHerinneringen();
			JOptionPane.showMessageDialog(null, "Herinneringen zijn Gestuurd");
		}
	}
	
	public void configureButton(JButton b) {
		b.setBackground(GLConfiguratie.getvButtonBackground());
		b.setForeground(GLConfiguratie.getvButtonForeground());
		b.setPreferredSize(new Dimension(200,70));
		b.addActionListener(this);
	}
	
	public void refreshTable() {
		GLTp.fireTableDataChanged();
		GLTr.fireTableDataChanged();
		GLTt.fireTableDataChanged();
	}
}
