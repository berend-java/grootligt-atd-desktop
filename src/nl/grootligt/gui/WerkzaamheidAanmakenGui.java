package nl.grootligt.gui;

import java.awt.*;
import java.awt.event.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.*;

import net.miginfocom.swing.MigLayout;
import nl.grootligt.agenda.Agenda;
import nl.grootligt.agenda.Reparatie;
import nl.grootligt.utils.GLConfiguratie;

public class WerkzaamheidAanmakenGui extends JFrame implements ActionListener {
	/**
	 * @author Berend de Groot
	 */
	private static final long serialVersionUID = 1L;
	private JButton Aanmaken = new JButton("Aanmaken");
	private JLabel naamLabel = new JLabel("Naam"), omschrijvingLabel = new JLabel("Omschrijving"), beginDatumLabel = new JLabel("Begin Datum"), reparatieLabel = new JLabel("Reparatie");
	private JTextField naamTf = new JTextField(15), omschrijvingTf = new JTextField(15), beginDatumtf = new JTextField(15);
	private JComboBox<?> reparaties;

	public WerkzaamheidAanmakenGui(Agenda a, AgendaGui aui) {
		Container p = getContentPane();
		MigLayout layout = new MigLayout();
		
		ArrayList<Reparatie> reparatiesA = a.getReparaties();
		Object[] cValues = reparatiesA.toArray();
		reparaties = new JComboBox<Object>(cValues);
		
		
		setTitle("ATD -> Klantenbinding -> Monteur Aanmaken");
		setSize(350, 230);
		setDefaultCloseOperation(HIDE_ON_CLOSE);
		setLayout(layout);
		setLocationRelativeTo(null);
		
		p.setBackground(GLConfiguratie.getFrameBackground());
		p.setForeground(Color.GREEN);
		beginDatumLabel.setForeground(GLConfiguratie.getvButtonForeground());
		omschrijvingLabel.setForeground(GLConfiguratie.getvButtonForeground());
		reparatieLabel.setForeground(GLConfiguratie.getvButtonForeground());
		naamLabel.setForeground(GLConfiguratie.getvButtonForeground());
		
		configureButton(Aanmaken);
		
		add(naamLabel, "cell 0 0");
		add(naamTf, "cell 1 0");
		add(omschrijvingLabel, "cell 0 1");
		add(omschrijvingTf, "cell 1 1");
		
		add(beginDatumLabel, "cell 0 2");
		add(beginDatumtf, "cell 1 2");
		add(reparatieLabel, "cell 0 3");
		add(reparaties, "cell 1 3");
		add(Aanmaken, "cell 0 5, gaptop 20");
		
        setVisible(true);  
	}
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == Aanmaken) {
			System.out.println("-> " + Aanmaken.getText());
			if(!naamTf.getText().equals("") && !omschrijvingTf.getText().equals("") && !beginDatumtf.getText().equals("")) {
				Date date2 = null;
				try {
					date2 = new SimpleDateFormat("dd-MM-yyyy").parse(beginDatumtf.getText());
				} catch (ParseException e1) {
					JOptionPane.showMessageDialog(null, "Datum graag in de volgende format: dd-MM-yyyy");
					return;
				}
				
				Reparatie r = (Reparatie) reparaties.getSelectedItem();
				r.addWerkzaamheid(naamTf.getText(), omschrijvingTf.getText(), date2);
				
				JOptionPane.showMessageDialog(null, "Werkzaamheid toegevoegd");
				this.dispose();
			}
		}
	}
	
	public void configureButton(JButton b) {
		b.setBackground(GLConfiguratie.getvButtonBackground());
		b.setForeground(GLConfiguratie.getvButtonForeground());
		b.setPreferredSize(new Dimension(100,30));
		b.addActionListener(this);
	}
}
