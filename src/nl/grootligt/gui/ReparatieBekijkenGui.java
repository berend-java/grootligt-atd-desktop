package nl.grootligt.gui;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import net.miginfocom.swing.MigLayout;
import nl.grootligt.agenda.Agenda;
import nl.grootligt.agenda.Reparatie;
import nl.grootligt.agenda.Werkzaamheid;
import nl.grootligt.utils.GLConfiguratie;

public class ReparatieBekijkenGui extends JFrame implements ActionListener {
	/**
	 * @author Berend de Groot
	 */
	private static final long serialVersionUID = 1L;
	private JLabel naam, naamLabel, reparatieLabel, statusLabel, status, werkzaamhedenLabel, werkzaamheden;
	private JComboBox<?> reparaties;
	
	public ReparatieBekijkenGui(Agenda a) {
		Container p = getContentPane();
		MigLayout layout = new MigLayout();
		
		setTitle("ATD -> VoorraadBeheer -> Reparatie bekijken");
		setSize(400, 300);
		setDefaultCloseOperation(HIDE_ON_CLOSE);
		setLayout(layout);
		setLocationRelativeTo(null);
		setResizable(false);
		
		Object[] cValues = a.getReparaties().toArray();
		reparaties = new JComboBox<Object>(cValues);
		reparaties.addActionListener(this);
		
		naam = new JLabel();
		status = new JLabel();
		werkzaamheden = new JLabel();
		
		reparatieLabel = new JLabel("Kies Reparatie");
		naamLabel = new JLabel("Naam");
		statusLabel = new JLabel("Status");
		werkzaamhedenLabel = new JLabel("Werkzaamheden");
		
		
		p.setBackground(GLConfiguratie.getFrameBackground());
		p.setForeground(Color.GREEN);
		naam.setForeground(GLConfiguratie.getvButtonForeground());
		naamLabel.setForeground(GLConfiguratie.getvButtonForeground());
		reparatieLabel.setForeground(GLConfiguratie.getvButtonForeground());
		status.setForeground(GLConfiguratie.getvButtonForeground());
		statusLabel.setForeground(GLConfiguratie.getvButtonForeground());
		werkzaamheden.setForeground(GLConfiguratie.getvButtonForeground());
		werkzaamhedenLabel.setForeground(GLConfiguratie.getvButtonForeground());

		add(reparatieLabel, "cell 0 0");
		add(reparaties, "cell 1 0");
		add(naamLabel, "cell 0 1");
		add(naam, "cell 1 1");
		add(statusLabel, "cell 0 2");
		add(status, "cell 1 2");
		add(werkzaamhedenLabel, "cell 0 3");
		add(werkzaamheden, "cell 1 3");
		
		
        setVisible(true);  
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == reparaties) {
			Reparatie r = (Reparatie) reparaties.getSelectedItem();
			naam.setText(r.getNaam());
			status.setText(r.getStatus());
			String s = "";
			for(Werkzaamheid w :r.getDeWerkzaamheden()) {
				s += w.getNaam() + " ";
			}
			werkzaamheden.setText(s);
			
		}
		
	}
}
