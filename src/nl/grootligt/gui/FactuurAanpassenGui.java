package nl.grootligt.gui;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

import javax.swing.*;

import net.miginfocom.swing.MigLayout;
import nl.grootligt.facturatie.Deurwaarder;
import nl.grootligt.facturatie.Factuur;
import nl.grootligt.facturatie.ReparatieFactuur;
import nl.grootligt.utils.GLConfiguratie;


public class FactuurAanpassenGui extends JFrame implements ActionListener {
	/**
	 * @author Berend de Groot
	 */
	private static final long serialVersionUID = 1L;
	private JButton aanpassen = new JButton("Aanpassen");
	private JLabel isBetaaldLabel = new JLabel("IsBetaaald"), isGereedLabel = new JLabel("isGereed"), factuurLabel = new JLabel("Kies Factuur");
	private JTextField isBetaaldTf = new JTextField(25), isGereedTf = new JTextField(25);
	private Object[] cValues, cValues1;
	private JComboBox<?> facturen;
	private FacturatieGui fgui;
	private Deurwaarder d;

	public FactuurAanpassenGui(FacturatieGui fgui, Deurwaarder d) {
		this.fgui = fgui;
		this.d = d;
		Container p = getContentPane();
		MigLayout layout = new MigLayout();
		
		ArrayList<ReparatieFactuur> rep = d.getReparatieFacturen();
		cValues = rep.toArray();
		
		facturen = new JComboBox<Object>(cValues);
		facturen.addActionListener(this);
		
		
		setTitle("ATD -> Factuur -> Factuur Aanpassen");
		setSize(350, 230);
		setDefaultCloseOperation(HIDE_ON_CLOSE);
		setLayout(layout);
		setLocationRelativeTo(null);
		
		p.setBackground(GLConfiguratie.getFrameBackground());
		p.setForeground(Color.GREEN);
		isBetaaldLabel.setForeground(GLConfiguratie.getvButtonForeground());
		isGereedLabel.setForeground(GLConfiguratie.getvButtonForeground());
		factuurLabel.setForeground(GLConfiguratie.getvButtonForeground());
		
		configureButton(aanpassen);
	
		add(factuurLabel, "cell 0 0");
		add(facturen, "cell 1 0");
		add(isBetaaldLabel,"cell 0 1");
		add(isBetaaldTf,"cell 1 1");
		add(isGereedLabel,"cell 0 2");
		add(isGereedTf,"cell 1 2");
		add(aanpassen, "cell 0 3");
		
        setVisible(true);  
	}
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == aanpassen) {
			System.out.println("-> " + aanpassen.getText());
			if(!isBetaaldTf.getText().equals("") && !isGereedTf.getText().equals("")) {
			    ReparatieFactuur b = (ReparatieFactuur) facturen.getSelectedItem();
			    
			    if(isBetaaldTf.getText().equals("true")) {
			    	b.setBetaald(true);
			    }
			    else {
			    	b.setBetaald(false);
			    }
			    
			    if(isGereedTf.getText().equals("true")) {
			    	b.setGereed(true);
			    }
			    else {
			    	b.setGereed(false);
			    }
			    
				JOptionPane.showMessageDialog(null, "Factuur Aangepast");
				fgui.refreshTable();
				this.dispose();
			}
		}
		else if (e.getSource() == facturen) {
			ReparatieFactuur r = (ReparatieFactuur) facturen.getSelectedItem();
			isBetaaldTf.setText("" + r.isBetaald());
			isGereedTf.setText("" + r.isGereed());
		}
	}
	
	public void configureButton(JButton b) {
		b.setBackground(GLConfiguratie.getvButtonBackground());
		b.setForeground(GLConfiguratie.getvButtonForeground());
		b.setPreferredSize(new Dimension(100,30));
		b.addActionListener(this);
	}
}

