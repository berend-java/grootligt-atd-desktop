package nl.grootligt.gui;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

import javax.swing.*;

import net.miginfocom.swing.MigLayout;
import nl.grootligt.klantenbinding.Klandizie;
import nl.grootligt.klantenbinding.Klant;
import nl.grootligt.parkeergarage.Auto;
import nl.grootligt.utils.GLConfiguratie;

public class HerinneringGenererenGui extends JFrame implements ActionListener {
	/**
	 * @author Berend de Groot
	 */
	private static final long serialVersionUID = 1L;
	private JButton opslaan = new JButton("Opslaan");
	private JLabel label1 = new JLabel("Klant Herinnering:"), label2 = new JLabel("Auto Herinnering:");
	private Klandizie k;
	private JPanel panel, panel2;
	private ArrayList<Auto> autos;
	private ArrayList<Klant> klant;

	@SuppressWarnings("unchecked")
	public HerinneringGenererenGui(Klandizie k) {
		this.k = k;
		Container p = getContentPane();
		MigLayout layout = new MigLayout();
		
		setTitle("ATD -> Klantenbinding -> Herinnering Genereren");
		setSize(350, 425);
		setDefaultCloseOperation(HIDE_ON_CLOSE);
		setLayout(layout);
		setLocationRelativeTo(null);
		setResizable(false);
		
		p.setBackground(GLConfiguratie.getFrameBackground());
		p.setForeground(Color.GREEN);
		label1.setForeground(GLConfiguratie.getvButtonForeground());
		label2.setForeground(GLConfiguratie.getvButtonForeground());
		
		configureButton(opslaan);
		autos = k.genereerAOMelding();
		klant = k.genereerLNGMelding();
		
		JScrollPane sp = new JScrollPane(panel);
		JScrollPane sp2 = new JScrollPane(panel2);
		
		sp.setBorder(BorderFactory.createEmptyBorder());
		sp.getViewport().setBackground(GLConfiguratie.getFrameBackground());
		sp.getViewport().setBorder(null);
		sp.setMinimumSize(new Dimension(200, 150));
		sp2.setBorder(BorderFactory.createEmptyBorder());
		sp2.getViewport().setBackground(GLConfiguratie.getFrameBackground());
		sp2.getViewport().setBorder(null);
		sp2.setMinimumSize(new Dimension(200, 150));
		
		
		
		JPanel panel = new JPanel(new FlowLayout());
		panel.setMinimumSize(new Dimension(200, 150));
		panel.setBackground(GLConfiguratie.getFrameBackground());
		JPanel panel2 = new JPanel(new FlowLayout());
		panel2.setMinimumSize(new Dimension(200, 150));
		panel2.setBackground(GLConfiguratie.getFrameBackground());
		
		for(Auto a: autos) {
			JLabel l = new JLabel(a.toString() + ",");
			l.setForeground(GLConfiguratie.getvButtonForeground());
			panel2.add(l);
		}
		
		for(Klant kk: klant) {
			JLabel l = new JLabel(kk.toString() + ",");
			l.setForeground(GLConfiguratie.getvButtonForeground());
			panel.add(l);
		}
		
		add(label1, "wrap 10");
		add(panel, "wrap");
		add(label2, "wrap 10");
		add(panel2, "wrap");
		add(opslaan, "dock south");
		
        setVisible(true);  
	}
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == opslaan) {
			System.out.println("-> " + opslaan.getText());
			k.sendAOMelding(autos);
			k.sendLNGMelding(klant);
			
			JOptionPane.showMessageDialog(null, "Herinneringen Opgeslagen");
			this.dispose();
		}
	}
	
	public void configureButton(JButton b) {
		b.setBackground(GLConfiguratie.getvButtonBackground());
		b.setForeground(GLConfiguratie.getvButtonForeground());
		b.setPreferredSize(new Dimension(100,30));
		b.addActionListener(this);
	}
}
