package nl.grootligt.gui;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

import javax.swing.*;

import net.miginfocom.swing.MigLayout;
import nl.grootligt.agenda.Agenda;
import nl.grootligt.agenda.Reparatie;
import nl.grootligt.klantenbinding.Klandizie;
import nl.grootligt.parkeergarage.Garage;
import nl.grootligt.utils.GLConfiguratie;

public class AanpassenReparatieGui extends JFrame implements ActionListener {
	/**
	 * @author Berend de Groot
	 */
	private static final long serialVersionUID = 1L;
	private JButton aanpassen = new JButton("Aanpassen");
	private JLabel naamLabel = new JLabel("Naam"), kiesReparatie = new JLabel("Kies Reparatie"), statusLabel = new JLabel("Stauts");
	private JTextField naamTf = new JTextField(15);
	private Object[] cValues, statussen =  {"Bezig", "Afgehandeld", "Geannuleerd"};
	private AgendaGui aui;
	private JComboBox<?> reparaties, status;
	private Reparatie reparatie = null;
	
	public AanpassenReparatieGui(Agenda a, Klandizie k, Garage g, AgendaGui aui) {
		this.aui = aui;
		Container p = getContentPane();
		MigLayout layout = new MigLayout();
		
		ArrayList<Reparatie> deReparaties = a.getReparaties();
		cValues = deReparaties.toArray();
		reparaties = new JComboBox<Object>(cValues);
		reparaties.addActionListener(this);

		status = new JComboBox<Object>(statussen);
		
		setTitle("ATD -> Klantenbinding -> Reparatie Aanpassen");
		setSize(350, 230);
		setDefaultCloseOperation(HIDE_ON_CLOSE);
		setLayout(layout);
		setLocationRelativeTo(null);
		
		p.setBackground(GLConfiguratie.getFrameBackground());
		p.setForeground(Color.GREEN);
		kiesReparatie.setForeground(GLConfiguratie.getvButtonForeground());
		naamLabel.setForeground(GLConfiguratie.getvButtonForeground());
		statusLabel.setForeground(GLConfiguratie.getvButtonForeground());
		
		configureButton(aanpassen);
		
		add(kiesReparatie, "cell 0 0");
		add(reparaties, "cell 1 0");
		add(naamLabel, "cell 0 1");
		add(naamTf, "cell 1 1");
		add(statusLabel, "cell 0 2");
		add(status, "cell 1 2");
		add(aanpassen, "cell 0 5, gaptop 20");
		
        setVisible(true);  
	}
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == aanpassen) {
			System.out.println("-> " + aanpassen.getText());
			if(!naamTf.equals("")) {
				reparatie.setNaam(naamTf.getText());
				reparatie.setStatus((String)status.getSelectedItem());
				if(status.getSelectedItem().equals("Afgehandeld")) {
					//reparatie.finish();
				}
				aui.refreshTable();
				JOptionPane.showMessageDialog(null, "Reparatie Aangepast");
				this.dispose();
			}
		}
		else if(e.getSource() == reparaties) {
			reparatie = (Reparatie) reparaties.getSelectedItem();
			naamTf.setText(reparatie.getNaam());
		}
	}
	
	public void configureButton(JButton b) {
		b.setBackground(GLConfiguratie.getvButtonBackground());
		b.setForeground(GLConfiguratie.getvButtonForeground());
		b.setPreferredSize(new Dimension(100,30));
		b.addActionListener(this);
	}
}
