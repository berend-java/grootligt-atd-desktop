package nl.grootligt.gui;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

import javax.swing.*;

import net.miginfocom.swing.MigLayout;
import nl.grootligt.klantenbinding.Klandizie;
import nl.grootligt.klantenbinding.Klant;
import nl.grootligt.parkeergarage.Garage;
import nl.grootligt.utils.GLConfiguratie;
import nl.grootligt.utils.GLTableModelKlant;

public class KlantenbindingGui extends JFrame implements ActionListener {
	/**
	 * @author Berend de Groot
	 */
	private static final long serialVersionUID = 1L;
	private JButton herinneringGenereren, herinneringSturen, herinneringReparatie, klantMaken, klantAanpassen, koppelAuto;
	private Klandizie k;
	private Garage g;
	private GLTableModelKlant GLTable;

	public KlantenbindingGui(Klandizie k, Garage g) {
		this.k = k;
		this.g = g;
		Container p = getContentPane();
		MigLayout layout = new MigLayout();
		
		setTitle("Auto Totaal Diensten -> Klantenbinding");
		setSize(1000, 420);
		setDefaultCloseOperation(HIDE_ON_CLOSE);
		setLayout(layout);
		setLocationRelativeTo(null);
		setResizable(false);
		
		p.setBackground(GLConfiguratie.getFrameBackground());

		herinneringGenereren = new JButton("Herinnering Genereren");
		herinneringSturen = new JButton("Herinneringen Sturen");
		klantMaken = new JButton("Klant Aanmaken");
		klantAanpassen = new JButton("Klant Aanpassen");
		koppelAuto = new JButton("Koppel Auto");
		
		configureButton(herinneringGenereren);
		configureButton(herinneringSturen);
		configureButton(klantMaken);
		configureButton(klantAanpassen);
		configureButton(koppelAuto);

		String[] columnNames = {"Voornaam", "Achternaam", "Email", " Geboorte Datum ", "Aantal Auto's"};
		ArrayList<Klant> data = k.getDeKlanten();
		GLTable = new GLTableModelKlant(columnNames, data);
		JTable table = new JTable(GLTable);

		table.setMinimumSize(new Dimension(700, 550));
		table.getTableHeader().setBackground(GLConfiguratie.getvTableBackgrid());
		table.getTableHeader().setForeground(GLConfiguratie.getvButtonForeground());
		table.setBackground(GLConfiguratie.getFrameBackground());
		table.setForeground(GLConfiguratie.getvButtonForeground());
		table.setGridColor(GLConfiguratie.getvTableBackgrid());
		table.setSelectionBackground(GLConfiguratie.getvButtonBackground());
		table.setSelectionForeground(Color.WHITE);

		JScrollPane sp = new JScrollPane(table);
		
		sp.setBorder(BorderFactory.createEmptyBorder());
		sp.getViewport().setBackground(GLConfiguratie.getFrameBackground());
		sp.getViewport().setBorder(null);
		sp.setMinimumSize(new Dimension(700, 550));

		JComponent comp = new JPanel(layout);
		comp.setBackground(GLConfiguratie.getFrameBackground());
		comp.setPreferredSize(new Dimension(500,500));
		add(sp, "dock west, gapright 60, gaptop 8, gapleft 8");
		comp.add(herinneringGenereren, "wrap");
		comp.add(herinneringSturen, "wrap");
		comp.add(klantMaken, "wrap");
		comp.add(klantAanpassen, "wrap");
		comp.add(koppelAuto);
		add(comp, "dock east");

        setVisible(true);  
	}
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == herinneringGenereren) {
			new HerinneringGenererenGui(k);
			System.out.println("-> " + herinneringGenereren.getText());
		}
		else if(e.getSource() == herinneringSturen) {
			System.out.println("-> " + herinneringSturen.getText());
			JOptionPane.showMessageDialog(null, "Herinneringen zijn 'verstuurd'.");
		}
		else if(e.getSource() == herinneringReparatie) {
			System.out.println("-> " + herinneringReparatie.getText());
		}
		else if(e.getSource() == klantMaken) {
			new KlantAanmakenGui(k, this);
			System.out.println("-> " + klantMaken.getText());
		}
		else if(e.getSource() == klantAanpassen) {
			new KlantAanpassenGui(k, this);
			System.out.println("-> " + klantAanpassen.getText());
		}
		else if(e.getSource() == koppelAuto) {
			new KoppelAutoGui(k, g, this);
			System.out.println("-> " + koppelAuto.getText());
		}
	}
	
	public void configureButton(JButton b) {
		b.setBackground(GLConfiguratie.getvButtonBackground());
		b.setForeground(GLConfiguratie.getvButtonForeground());
		b.setPreferredSize(new Dimension(200,70));
		b.addActionListener(this);
	}
	public void refreshTable() {
		GLTable.fireTableDataChanged();
	}
}
