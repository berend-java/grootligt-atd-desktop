package nl.grootligt.gui;

import java.awt.*;
import java.awt.event.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.*;

import net.miginfocom.swing.MigLayout;
import nl.grootligt.klantenbinding.Klandizie;
import nl.grootligt.utils.GLConfiguratie;

public class KlantAanmakenGui extends JFrame implements ActionListener {
	/**
	 * @author Berend de Groot
	 */
	private static final long serialVersionUID = 1L;
	private JButton aanmaken = new JButton("Aanmaken");
	private JLabel voornaamLabel = new JLabel("Voornaam"), achternaamLabel = new JLabel("Achternaam"), emailLabel = new JLabel("Email"), geboorteLabel = new JLabel("Geboorte Datum");
	private JTextField voornaamTf = new JTextField(25), achternaamTf = new JTextField(25), emailTf = new JTextField(25), geboorteTf = new JTextField(25);
	private Klandizie k;
	private KlantenbindingGui kui;

	public KlantAanmakenGui(Klandizie k, KlantenbindingGui kui) {
		this.k = k;
		this.kui = kui;
		Container p = getContentPane();
		MigLayout layout = new MigLayout();
		
		setTitle("ATD -> Klantenbinding -> Herinnering Genereren");
		setSize(350, 200);
		setDefaultCloseOperation(HIDE_ON_CLOSE);
		setLayout(layout);
		setLocationRelativeTo(null);
		
		p.setBackground(GLConfiguratie.getFrameBackground());
		p.setForeground(Color.GREEN);
		voornaamLabel.setForeground(GLConfiguratie.getvButtonForeground());
		achternaamLabel.setForeground(GLConfiguratie.getvButtonForeground());
		emailLabel.setForeground(GLConfiguratie.getvButtonForeground());
		geboorteLabel.setForeground(GLConfiguratie.getvButtonForeground());
		
		configureButton(aanmaken);
		
		add(voornaamLabel, "cell 0 0");
		add(voornaamTf, "cell 1 0");
		add(achternaamLabel, "cell 0 1 ");
		add(achternaamTf, "cell 1 1");
		add(emailLabel, "cell 0 2");
		add(emailTf, "cell 1 2");
		add(geboorteLabel, "cell 0 3");
		add(geboorteTf, "cell 1 3");
		add(aanmaken, "cell 0 4, gaptop 20");
		
		
        setVisible(true);  
	}
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == aanmaken) {
			System.out.println("-> " + aanmaken.getText());
			if(!voornaamTf.getText().equals("") && !achternaamTf.getText().equals("") && !emailTf.getText().equals("") && !geboorteTf.getText().equals("")) {
				Date date = null;
				try {
					date = new SimpleDateFormat("dd-MM-yyyy").parse(geboorteTf.getText());
				} catch (ParseException e1) {
					JOptionPane.showMessageDialog(null, "Datum graag in de volgende format: dd-MM-yyyy");
					return;
				}
				k.addKlant(voornaamTf.getText(), achternaamTf.getText(), emailTf.getText(), date);	
				kui.refreshTable();
				
				JOptionPane.showMessageDialog(null, "Klant toegevoegd");
				this.dispose();
			}
		}
	}
	
	public void configureButton(JButton b) {
		b.setBackground(GLConfiguratie.getvButtonBackground());
		b.setForeground(GLConfiguratie.getvButtonForeground());
		b.setPreferredSize(new Dimension(100,30));
		b.addActionListener(this);
	}
}
