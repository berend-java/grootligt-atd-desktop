package nl.grootligt.gui;

import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import net.miginfocom.swing.MigLayout;
import nl.grootligt.klantenbinding.Klandizie;
import nl.grootligt.klantenbinding.Klant;
import nl.grootligt.parkeergarage.Auto;
import nl.grootligt.parkeergarage.Garage;
import nl.grootligt.utils.GLConfiguratie;

public class KoppelAutoGui extends JFrame implements ActionListener {
	/**
	 * @author Berend de Groot
	 */
	private static final long serialVersionUID = 1L;
	private JLabel auto = new JLabel("Kies Auto"), klant = new JLabel("Kies Klant");
	private JButton koppel = new JButton("Koppel");
	private Object[] cValues, cValues2;
	private JComboBox<?> klanten, autos;
	private MigLayout layout = new MigLayout();
	private Klandizie k;
	private KlantenbindingGui kbUi;
	
	public KoppelAutoGui(Klandizie k, Garage g, KlantenbindingGui kbUi) {
		this.k = k;
		this.kbUi = kbUi;

		cValues = g.getAutosZonderEigenaar().toArray();
		cValues2 = k.getDeKlanten().toArray();
		
		autos = new JComboBox<Object>(cValues);
		klanten = new JComboBox<Object>(cValues2);
		
		Container p = getContentPane();
		
		setTitle("ATD -> Voorraad Beheer -> Koppel Auto");
		setSize(250, 220);
		setDefaultCloseOperation(HIDE_ON_CLOSE);
		setLayout(layout);
		setLocationRelativeTo(null);
		setResizable(false);
		
		p.setBackground(GLConfiguratie.getFrameBackground());
		p.setForeground(Color.GREEN);
		klant.setForeground(GLConfiguratie.getvButtonForeground());
		auto.setForeground(GLConfiguratie.getvButtonForeground());
		
		configureButton(koppel);
		
		add(klant);
		add(klanten, "wrap 40, gaptop 20");
		add(auto);
		add(autos, "wrap 40");
		add(koppel, "dock south");
		
		setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == koppel) {
			System.out.println("-> " + koppel.getText());
			Auto dAuto = (Auto) autos.getSelectedItem();
			Klant dKlant = (Klant) klanten.getSelectedItem();
			boolean b = k.koppelAutoKlant(dAuto, dKlant);
				
			if(b) {
				JOptionPane.showMessageDialog(null, "Klant en Auto Gekoppeld");
				this.dispose();
			} else {
				JOptionPane.showMessageDialog(null, "Klant en Auto niet Gekoppeld");
			}
			kbUi.refreshTable();
		}
	}
	
	public void configureButton(JButton b) {
		b.setBackground(GLConfiguratie.getvButtonBackground());
		b.setForeground(GLConfiguratie.getvButtonForeground());
		//b.setMinimumSize(new Dimension(100,30));
		b.addActionListener(this);
	}
}
