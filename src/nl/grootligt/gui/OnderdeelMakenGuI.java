package nl.grootligt.gui;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import net.miginfocom.swing.MigLayout;
import nl.grootligt.utils.GLConfiguratie;
import nl.grootligt.voorraadbeheer.Magazijn;

public class OnderdeelMakenGuI extends JFrame implements ActionListener {
	/**
	 * @author Berend de Groot
	 */
	private static final long serialVersionUID = 1L;
	private JButton aanmaken = new JButton("Aanmaken");
	private JLabel artikelnummerLabel = new JLabel("Artikel Nummer"), prijsLabel = new JLabel("Pijs"), typeLabel = new JLabel("Type");
	private JTextField artiekelnummerTf = new JTextField(25), prijsTf = new JTextField(25), typeTf = new JTextField(25);
	private Magazijn m;
	private VoorraadBeheerGui j;
	
	public OnderdeelMakenGuI(Magazijn m, VoorraadBeheerGui j) {
		this.m = m;
		this.j = j;
		Container p = getContentPane();
		MigLayout layout = new MigLayout();
		
		setTitle("ATD -> VoorraadBeheer -> Onderdeel Aanmaken");
		setSize(350, 170);
		setDefaultCloseOperation(HIDE_ON_CLOSE);
		setLayout(layout);
		setLocationRelativeTo(null);
		setResizable(false);
		
		p.setBackground(GLConfiguratie.getFrameBackground());
		p.setForeground(Color.GREEN);
		artikelnummerLabel.setForeground(GLConfiguratie.getvButtonForeground());
		prijsLabel.setForeground(GLConfiguratie.getvButtonForeground());
		typeLabel.setForeground(GLConfiguratie.getvButtonForeground());
		
		configureButton(aanmaken);
		
		add(artikelnummerLabel, "cell 0 0");
		add(artiekelnummerTf, "cell 1 0");
		add(prijsLabel, "cell 0 1 ");
		add(prijsTf, "cell 1 1");
		add(typeLabel, "cell 0 2");
		add(typeTf, "cell 1 2");
		add(aanmaken, "cell 0 4, gaptop 20");
		
		
        setVisible(true);  
	}
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == aanmaken) {
			System.out.println("-> " + aanmaken.getText());
			if(!artiekelnummerTf.getText().equals("") && !prijsTf.getText().equals("") && !typeTf.getText().equals("")) {
				int an = 0;
				double prijs = 0;
				try {
					an = Integer.parseInt(artiekelnummerTf.getText());
					prijs = Double.parseDouble(prijsTf.getText());
					
					if(prijs < 0) {
						JOptionPane.showMessageDialog(null, "Prijs moet hoger als 0 zijn.");
						return;
					}
					
				} catch (NumberFormatException ex) {
					JOptionPane.showMessageDialog(null, "Prijs en Artikelnummer moeten getallen zijn.");
					return;
				}
				String type = typeTf.getText();
		
				boolean b = m.addOnderdeel(an, 0, prijs, type);
				if(b) {
					JOptionPane.showMessageDialog(null, "Onderdeel toegevoegd");
					this.dispose();
				} else {
					JOptionPane.showMessageDialog(null, "Onderdeel niet toegevoegd");
				}
				j.refreshTable();
			}
		}
	}
	
	public void configureButton(JButton b) {
		b.setBackground(GLConfiguratie.getvButtonBackground());
		b.setForeground(GLConfiguratie.getvButtonForeground());
		b.setPreferredSize(new Dimension(100,30));
		b.addActionListener(this);
	}
}
