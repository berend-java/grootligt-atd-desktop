package nl.grootligt.gui;

import java.awt.*;
import java.awt.event.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import javax.swing.*;

import net.miginfocom.swing.MigLayout;
import nl.grootligt.klantenbinding.Klandizie;
import nl.grootligt.klantenbinding.Klant;
import nl.grootligt.parkeergarage.Garage;
import nl.grootligt.utils.GLConfiguratie;
import nl.grootligt.voorraadbeheer.Brandstof;
import nl.grootligt.voorraadbeheer.Magazijn;

public class AutoAanmakenGui extends JFrame implements ActionListener {
	/**
	 * @author Berend de Groot
	 */
	private static final long serialVersionUID = 1L;
	private JButton aanpassen = new JButton("Aanmaken");
	private JLabel typeLabel = new JLabel("Type"), merkLabel = new JLabel("Merk"), brandstofLabel = new JLabel("brandstof"), kentekenLabel = new JLabel("Kenteken");
	private JTextField kentekenTf = new JTextField(25), merkTf = new JTextField(25), typeTf = new JTextField(25);
	private Object[] cValues;
	private KlantenbindingGui kui;
	private JComboBox<?> brandstofc;
	private Magazijn m;
	private Garage g;

	public AutoAanmakenGui(Magazijn m, Garage g) {
		this.m = m;
		this.g =g;
		Container p = getContentPane();
		MigLayout layout = new MigLayout();
		
		ArrayList<Brandstof> brandstof = m.getDeBrandstoffen();
		cValues = brandstof.toArray();
		
		brandstofc = new JComboBox<Object>(cValues);
		
		setTitle("ATD -> ParkeerGarage -> Auto Aanmaken");
		setSize(350, 230);
		setDefaultCloseOperation(HIDE_ON_CLOSE);
		setLayout(layout);
		setLocationRelativeTo(null);
		
		p.setBackground(GLConfiguratie.getFrameBackground());
		p.setForeground(Color.GREEN);
		typeLabel.setForeground(GLConfiguratie.getvButtonForeground());
		merkLabel.setForeground(GLConfiguratie.getvButtonForeground());
		brandstofLabel.setForeground(GLConfiguratie.getvButtonForeground());
		kentekenLabel.setForeground(GLConfiguratie.getvButtonForeground());
		
		configureButton(aanpassen);
	
		add(kentekenLabel,"cell 0 0");
		add(kentekenTf,"cell 1 0");
		add(typeLabel,"cell 0 1");
		add(typeTf,"cell 1 1");
		add(merkLabel,"cell 0 2");
		add(merkTf,"cell 1 2");
		add(brandstofLabel,"cell 0 3");
		add(brandstofc,"cell 1 3");
		add(aanpassen, "cell 0 4");
		
        setVisible(true);  
	}
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == aanpassen) {
			System.out.println("-> " + aanpassen.getText());
			if(!kentekenTf.getText().equals("") && !merkTf.getText().equals("") && !typeTf.getText().equals("")) {
			    Object b = brandstofc.getSelectedItem();
			    String s = b.toString();
				g.addAuto(typeTf.getText(), merkTf.getText(), s, kentekenTf.getText());
				
				JOptionPane.showMessageDialog(null, "Auto Aangemaakt");
				this.dispose();
			}
		}
	}
	
	public void configureButton(JButton b) {
		b.setBackground(GLConfiguratie.getvButtonBackground());
		b.setForeground(GLConfiguratie.getvButtonForeground());
		b.setPreferredSize(new Dimension(100,30));
		b.addActionListener(this);
	}
}

