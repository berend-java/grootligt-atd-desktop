package nl.grootligt.gui;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

import javax.swing.*;

import net.miginfocom.swing.MigLayout;
import nl.grootligt.utils.GLConfiguratie;
import nl.grootligt.utils.GLTableModelBrandstof;
import nl.grootligt.utils.GLTableModelOnderdeel;
import nl.grootligt.voorraadbeheer.Brandstof;
import nl.grootligt.voorraadbeheer.Magazijn;
import nl.grootligt.voorraadbeheer.Onderdeel;

public class VoorraadBeheerGui extends JFrame implements ActionListener {
	/**
	 * @author Berend de Groot
	 */
	private static final long serialVersionUID = 1L;
	private JButton onderdelenBestellen, brandstofBestellen, onderdeelMaken;
	private JTable table, table2;
	private String[] columnNames = {"Artknr", "Aantal", "Prijs", "Type"};
	private String[] columnNames2 = {"TSIC", "Aantal", "Prijs", "Type"};
	private Magazijn m;
	private ArrayList<Onderdeel> deOnderdelen;
	private ArrayList<Brandstof> deBrandstoffen;
	private GLTableModelOnderdeel GLTMo;
	private GLTableModelBrandstof GLMb;

	public VoorraadBeheerGui(Magazijn m) {
		this.m = m;
		Container p = getContentPane();
		MigLayout layout = new MigLayout();
		
		setTitle("Auto Totaal Diensten -> Voorraad Beheer");
		setSize(800, 400);
		setDefaultCloseOperation(HIDE_ON_CLOSE);
		setLayout(layout);
		setLocationRelativeTo(null);
		setResizable(false);
		
		p.setBackground(GLConfiguratie.getFrameBackground());

		onderdelenBestellen = new JButton("Onderdelen Bestellen");
		brandstofBestellen = new JButton("Brandstof Bestellen");
		onderdeelMaken = new JButton("Onderdeel Maken");
		
		configureButton(onderdelenBestellen);
		configureButton(brandstofBestellen);
		configureButton(onderdeelMaken);

		deOnderdelen = m.getDeOnderdelen();
		deBrandstoffen = m.getDeBrandstoffen();
		
		GLTMo = new GLTableModelOnderdeel(columnNames, deOnderdelen);
		GLMb = new GLTableModelBrandstof(columnNames2, deBrandstoffen);
		table = new JTable(GLTMo);
		table2 = new JTable(GLMb);
		
		table.getTableHeader().setBackground(GLConfiguratie.getvTableBackgrid());
		table.getTableHeader().setForeground(GLConfiguratie.getvButtonForeground());
		table.setBackground(GLConfiguratie.getFrameBackground());
		table.setForeground(GLConfiguratie.getvButtonForeground());
		table.setGridColor(GLConfiguratie.getvTableBackgrid());
		table.setMinimumSize(new Dimension(450, 100));
		table.setSelectionBackground(GLConfiguratie.getvButtonBackground());
		table.setSelectionForeground(Color.WHITE);
		
		table2.getTableHeader().setBackground(GLConfiguratie.getvTableBackgrid());
		table2.getTableHeader().setForeground(Color.WHITE);
		table2.setBackground(GLConfiguratie.getFrameBackground());
		table2.setForeground(Color.WHITE);
		table2.setGridColor(GLConfiguratie.getvTableBackgrid());
		table2.setMinimumSize(new Dimension(450, 100));
		table2.setSelectionBackground(GLConfiguratie.getvButtonBackground());
		table2.setSelectionForeground(Color.WHITE);

		JScrollPane sp = new JScrollPane(table);
		JScrollPane sp2 = new JScrollPane(table2);
		JComponent tables = new JPanel(layout);
		tables.setBackground(GLConfiguratie.getFrameBackground());
		tables.setMinimumSize(new Dimension(450, 100));
		JComponent comp = new JPanel(layout);
		
		sp.setBorder(BorderFactory.createEmptyBorder());
		sp.getViewport().setBackground(GLConfiguratie.getFrameBackground());
		sp.getViewport().setBorder(null);
		sp.setMinimumSize(new Dimension(420, 100));
		
		sp2.setBorder(BorderFactory.createEmptyBorder());
		sp2.getViewport().setBackground(GLConfiguratie.getFrameBackground());
		sp2.getViewport().setBorder(null);
		sp2.setMinimumSize(new Dimension(435, 55));
		sp2.setMaximumSize(new Dimension(435, 55));
		
		tables.add(sp2, "wrap, gaptop 5, gapleft 5");
		tables.add(sp, "gapleft 5");
		comp.setBackground(GLConfiguratie.getFrameBackground());
		add(tables, "dock west, gapright 120");
		comp.add(onderdelenBestellen, "wrap");
		comp.add(brandstofBestellen, "wrap");
		comp.add(onderdeelMaken);
		add(comp, "dock east");

        setVisible(true);  
	}
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == onderdelenBestellen) {
			new OnderdeelBestellenGui(m, this);
			System.out.println("-> " + onderdelenBestellen.getText());
		}
		else if(e.getSource() == brandstofBestellen) {
			new BrandstofBestellenGui(m, this);
			System.out.println("-> " + brandstofBestellen.getText());
		}
		else if(e.getSource() == onderdeelMaken) {
			new OnderdeelMakenGuI(m, this);
		}
	}
	
	public void configureButton(JButton b) {
		b.setBackground(GLConfiguratie.getFrameForeground());
		b.setForeground(Color.WHITE);
		b.setPreferredSize(new Dimension(200,70));
		b.addActionListener(this);
	}
	
	public void refreshTable() {
		GLTMo.fireTableDataChanged();
		GLMb.fireTableDataChanged();
	}
}
