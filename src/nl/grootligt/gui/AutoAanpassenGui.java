package nl.grootligt.gui;

import java.awt.*;
import java.awt.event.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import javax.swing.*;

import net.miginfocom.swing.MigLayout;
import nl.grootligt.klantenbinding.Klandizie;
import nl.grootligt.klantenbinding.Klant;
import nl.grootligt.parkeergarage.Auto;
import nl.grootligt.parkeergarage.Garage;
import nl.grootligt.utils.GLConfiguratie;
import nl.grootligt.voorraadbeheer.Brandstof;
import nl.grootligt.voorraadbeheer.Magazijn;

public class AutoAanpassenGui extends JFrame implements ActionListener {
	/**
	 * @author Berend de Groot
	 */
	private static final long serialVersionUID = 1L;
	private JButton aanpassen = new JButton("Aanmaken");
	private JLabel typeLabel = new JLabel("Type"), merkLabel = new JLabel("Merk"), brandstofLabel = new JLabel("brandstof"), kentekenLabel = new JLabel("Kenteken"), autoLabel = new JLabel("Kies Auto");
	private JTextField kentekenTf = new JTextField(25), merkTf = new JTextField(25), typeTf = new JTextField(25);
	private Object[] cValues, cValues1;
	private KlantenbindingGui kui;
	private JComboBox<?> brandstofc, autos;
	private Magazijn m;
	private Garage g;

	public AutoAanpassenGui(Magazijn m, Garage g) {
		this.m = m;
		this.g =g;
		Container p = getContentPane();
		MigLayout layout = new MigLayout();
		
		ArrayList<Brandstof> brandstof = m.getDeBrandstoffen();
		cValues = brandstof.toArray();
		
		ArrayList<Auto> deautos = g.getAutos();
		cValues1 = deautos.toArray();
		
		autos = new JComboBox<Object>(cValues1);
		autos.addActionListener(this);
		
		brandstofc = new JComboBox<Object>(cValues);
		
		setTitle("ATD -> ParkeerGarage -> Auto Aanpassen");
		setSize(350, 230);
		setDefaultCloseOperation(HIDE_ON_CLOSE);
		setLayout(layout);
		setLocationRelativeTo(null);
		
		p.setBackground(GLConfiguratie.getFrameBackground());
		p.setForeground(Color.GREEN);
		typeLabel.setForeground(GLConfiguratie.getvButtonForeground());
		merkLabel.setForeground(GLConfiguratie.getvButtonForeground());
		brandstofLabel.setForeground(GLConfiguratie.getvButtonForeground());
		kentekenLabel.setForeground(GLConfiguratie.getvButtonForeground());
		autoLabel.setForeground(GLConfiguratie.getvButtonForeground());
		
		configureButton(aanpassen);
	
		add(autoLabel, "cell 0 0");
		add(autos, "cell 1 0");
		add(kentekenLabel,"cell 0 1");
		add(kentekenTf,"cell 1 1");
		add(typeLabel,"cell 0 2");
		add(typeTf,"cell 1 2");
		add(merkLabel,"cell 0 3");
		add(merkTf,"cell 1 3");
		add(brandstofLabel,"cell 0 4");
		add(brandstofc,"cell 1 4");
		add(aanpassen, "cell 0 5");
		
        setVisible(true);  
	}
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == aanpassen) {
			System.out.println("-> " + aanpassen.getText());
			if(!kentekenTf.getText().equals("") && !merkTf.getText().equals("") && !typeTf.getText().equals("")) {
			    Object b = brandstofc.getSelectedItem();
			    String s = b.toString();
				
			    Auto a = (Auto) autos.getSelectedItem();
			    a.setKenteken(kentekenTf.getText());
			    a.setMerk(merkTf.getText());
			    a.setType(typeTf.getText());
			    a.setBrandstof(s);
				
				JOptionPane.showMessageDialog(null, "Auto Aangepast");
				this.dispose();
			}
		}
		else if (e.getSource() == autos) {
			Auto a = (Auto) autos.getSelectedItem();
			kentekenTf.setText(a.getKenteken());
			merkTf.setText(a.getMerk());
			typeTf.setText(a.getType());
		}
	}
	
	public void configureButton(JButton b) {
		b.setBackground(GLConfiguratie.getvButtonBackground());
		b.setForeground(GLConfiguratie.getvButtonForeground());
		b.setPreferredSize(new Dimension(100,30));
		b.addActionListener(this);
	}
}

