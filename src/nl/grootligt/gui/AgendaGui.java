package nl.grootligt.gui;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

import javax.swing.*;

import net.miginfocom.swing.MigLayout;
import nl.grootligt.agenda.Agenda;
import nl.grootligt.agenda.Reparatie;
import nl.grootligt.klantenbinding.Klandizie;
import nl.grootligt.parkeergarage.Garage;
import nl.grootligt.utils.GLConfiguratie;
import nl.grootligt.utils.GLTableModelAgenda;
import nl.grootligt.voorraadbeheer.Magazijn;

public class AgendaGui extends JFrame implements ActionListener {
	/**
	 * @author Berend de Groot
	 */
	private static final long serialVersionUID = 1L;
	private JButton reparatieToevoegenButton, monteurToevoegenButton, werkzaamheidToevoegenButton, reparatieAaanpassenButton, goederenKoppelenButton, monteurKoppelenButton, reparatieBekijken, brandstofToevoegen;
	private Agenda a;
	private Klandizie k;
	private Garage g;
	private Magazijn m;
	private GLTableModelAgenda GLT;

	public AgendaGui(Agenda a, Klandizie k, Garage g, Magazijn m) {
		this.a = a;
		this.k = k;
		this.g = g;
		this.m = m;
		Container p = getContentPane();
		MigLayout layout = new MigLayout();
		
		setTitle("Auto Totaal Diensten -> Agenda");
		setSize(800, 600);
		setDefaultCloseOperation(HIDE_ON_CLOSE);
		setLayout(layout);
		setLocationRelativeTo(null);
		setResizable(false);
		
		p.setBackground(GLConfiguratie.getFrameBackground());
		p.setForeground(GLConfiguratie.getFrameForeground());

		reparatieToevoegenButton = new JButton("Reparatie Toevoegen");
		monteurToevoegenButton = new JButton("Monteur Toevoegen");
		werkzaamheidToevoegenButton = new JButton("Werkzaamheid Toevoegen");
		reparatieAaanpassenButton = new JButton("Reparatie Aanpassen");
		goederenKoppelenButton = new JButton("Goederen Koppelen");
		monteurKoppelenButton = new JButton("Monteur Koppelen");
		reparatieBekijken = new JButton("Reparatie Bekijken");
		brandstofToevoegen = new JButton("Brandstof Koppelen");
		
		configureButton(reparatieToevoegenButton);
		configureButton(monteurToevoegenButton);
		configureButton(werkzaamheidToevoegenButton);
		configureButton(reparatieAaanpassenButton);
		configureButton(goederenKoppelenButton);
		configureButton(monteurKoppelenButton);
		configureButton(reparatieBekijken);
		configureButton(brandstofToevoegen);

		String[] columnNames = {"Naam", "Monteurs", "Onderdelen", "Brandstof", "Status"};
					
		ArrayList<Reparatie> data = a.getReparaties();
		GLT = new GLTableModelAgenda(columnNames, data);
		JTable table = new JTable(GLT);
		
		table.getTableHeader().setBackground(GLConfiguratie.getvTableBackgrid());
		table.getTableHeader().setForeground(GLConfiguratie.getvButtonForeground());
		table.setBackground(GLConfiguratie.getFrameBackground());
		table.setForeground(GLConfiguratie.getvButtonForeground());
		table.setGridColor(GLConfiguratie.getvTableBackgrid());
		table.setMinimumSize(new Dimension(500, 100));
		table.setSelectionBackground(GLConfiguratie.getvButtonBackground());
		table.setSelectionForeground(Color.WHITE);

		JScrollPane sp = new JScrollPane(table);
		
		sp.setBorder(BorderFactory.createEmptyBorder());
		sp.getViewport().setBackground(GLConfiguratie.getFrameBackground());
		sp.getViewport().setBorder(null);
		sp.setMinimumSize(new Dimension(500, 550));
		
		add(sp, "span 0 8, gapright 70");
		add(reparatieToevoegenButton, "cell 1 0");
		add(monteurToevoegenButton, "cell 1 1");
		add(werkzaamheidToevoegenButton, "cell 1 2");
		add(reparatieAaanpassenButton, "cell 1 3");
		add(goederenKoppelenButton, "cell 1 4");
		add(brandstofToevoegen, "cell 1 5");
		add(monteurKoppelenButton, "cell 1 6");
		add(reparatieBekijken, "cell 1 7");

        setVisible(true);  
	}

	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == reparatieToevoegenButton) {
			System.out.println("-> " + reparatieToevoegenButton.getText());
			new ToevoegenReparatieGui(a, k, g, this);
		}
		else if(e.getSource() == monteurToevoegenButton) {
			System.out.println("-> " + monteurToevoegenButton.getText());
			new MonteurAanmakenGui(a, this);
		}
		else if(e.getSource() == werkzaamheidToevoegenButton) {
			System.out.println("-> " + werkzaamheidToevoegenButton.getText());
			new WerkzaamheidAanmakenGui(a, this);
		}
		else if(e.getSource() == reparatieAaanpassenButton) {
			System.out.println("-> " + reparatieAaanpassenButton.getText());
			new AanpassenReparatieGui(a, k, g, this);
		}
		else if(e.getSource() == goederenKoppelenButton) {
			System.out.println("-> " + goederenKoppelenButton.getText());
			new GoederenToevoegenReparatieGui(m, a, this);
		}
		else if(e.getSource() == monteurKoppelenButton) {
			System.out.println("-> " + monteurKoppelenButton.getText());
			new MonteurKoppelenGui(a, this);
		}
		else if(e.getSource() == reparatieBekijken) {
			System.out.println("-> " + reparatieBekijken.getText());
			new ReparatieBekijkenGui(a);
		}
		else if(e.getSource() == brandstofToevoegen) {
			System.out.println("-> " + brandstofToevoegen.getText());
			new BrandstofToevoegenReparatie(m, a, this);
		}
	}
	
	public void configureButton(JButton b) {
		b.setBackground(GLConfiguratie.getvButtonBackground());
		b.setForeground(GLConfiguratie.getvButtonForeground());
		b.setPreferredSize(new Dimension(200,70));
		b.addActionListener(this);
	}
	
	public void refreshTable() {
		GLT.fireTableDataChanged();
	}
}
