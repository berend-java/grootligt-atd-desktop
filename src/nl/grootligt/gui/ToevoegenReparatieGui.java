package nl.grootligt.gui;

import java.awt.*;
import java.awt.event.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.*;

import net.miginfocom.swing.MigLayout;
import nl.grootligt.agenda.Agenda;
import nl.grootligt.agenda.Monteur;
import nl.grootligt.agenda.Reparatie;
import nl.grootligt.klantenbinding.Klandizie;
import nl.grootligt.klantenbinding.Klant;
import nl.grootligt.parkeergarage.Auto;
import nl.grootligt.parkeergarage.Garage;
import nl.grootligt.utils.GLConfiguratie;

public class ToevoegenReparatieGui extends JFrame implements ActionListener {
	/**
	 * @author Berend de Groot
	 */
	private static final long serialVersionUID = 1L;
	private JButton aanpassen = new JButton("Aanmaken");
	private JLabel klantLabel = new JLabel("Kies Klant"), naamLabel = new JLabel("Naam"), datumLabel = new JLabel("Datum"), monteurLabel = new JLabel("Kies Monteur"), autoLabel = new JLabel("Kies Auto");
	private JTextField naamTf = new JTextField(20), datumTf = new JTextField(20);
	private Object[] cValues, cValues2, cValues3;
	private AgendaGui aui;
	private JComboBox<?> klanten, monteurs, autos;
	private Agenda a;
	
	public ToevoegenReparatieGui(Agenda a, Klandizie k, Garage g, AgendaGui aui) {
		this.a = a;
		this.aui = aui;
		Container p = getContentPane();
		MigLayout layout = new MigLayout();
		
		ArrayList<Klant> deKlanten = k.getDeKlanten();
		cValues = deKlanten.toArray();
		klanten = new JComboBox<Object>(cValues);
		
		ArrayList<Monteur> deMonteurs = a.getMonteurs();
		cValues2 = deMonteurs.toArray();
		monteurs = new JComboBox<Object>(cValues2);
		
		ArrayList<Auto> deAutos = g.getAutos();
		cValues3 = deAutos.toArray();
		autos = new JComboBox<Object>(cValues3);
		
		setTitle("ATD -> Klantenbinding -> Toevoegen Reparatie");
		setSize(350, 230);
		setDefaultCloseOperation(HIDE_ON_CLOSE);
		setLayout(layout);
		setLocationRelativeTo(null);
		
		p.setBackground(GLConfiguratie.getFrameBackground());
		p.setForeground(Color.GREEN);
		klantLabel.setForeground(GLConfiguratie.getvButtonForeground());
		naamLabel.setForeground(GLConfiguratie.getvButtonForeground());
		datumLabel.setForeground(GLConfiguratie.getvButtonForeground());
		monteurLabel.setForeground(GLConfiguratie.getvButtonForeground());
		autoLabel.setForeground(GLConfiguratie.getvButtonForeground());
		
		configureButton(aanpassen);
		
		add(klantLabel, "cell 0 0");
		add(klanten, "cell 1 0");
		add(naamLabel, "cell 0 1");
		add(naamTf, "cell 1 1");
		add(datumLabel, "cell 0 2 ");
		add(datumTf, "cell 1 2");
		add(monteurLabel, "cell 0 3");
		add(monteurs, "cell 1 3");
		add(autoLabel, "cell 0 4");
		add(autos, "cell 1 4");
		add(aanpassen, "cell 0 5, gaptop 20");
		
		
        setVisible(true);  
	}
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == aanpassen) {
			System.out.println("-> " + aanpassen.getText());
			if(!naamTf.equals("") && !datumTf.equals("")) {
				Date date = null;
				try {
					date = new SimpleDateFormat("dd-MM-yyyy").parse(datumTf.getText());
				} catch (ParseException e1) {
					JOptionPane.showMessageDialog(null, "Datum graag in de volgende format: dd-MM-yyyy");
					return;
				}
				a.addReparatie("Bezig", naamTf.getText(), date, (Auto) autos.getSelectedItem());
				Reparatie r = a.getReparatieByName(naamTf.getText());
				a.koppelMonRep((Monteur) monteurs.getSelectedItem(), r);
				
				aui.refreshTable();
				JOptionPane.showMessageDialog(null, "Reparatie toegevoegd");
				this.dispose();
			}
		}
	}
	
	public void configureButton(JButton b) {
		b.setBackground(GLConfiguratie.getvButtonBackground());
		b.setForeground(GLConfiguratie.getvButtonForeground());
		b.setPreferredSize(new Dimension(100,30));
		b.addActionListener(this);
	}
}
