package nl.grootligt.gui;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import javax.swing.*;
import net.miginfocom.swing.MigLayout;
import nl.grootligt.klantenbinding.Klandizie;
import nl.grootligt.parkeergarage.Garage;
import nl.grootligt.parkeergarage.Plek;
import nl.grootligt.utils.GLConfiguratie;

public class PlekVrijmakenGui extends JFrame implements ActionListener {
	/**
	 * @author Berend de Groot
	 */
	private static final long serialVersionUID = 1L;
	private JButton aanpassen = new JButton("Vrijmaken");
	private JLabel plekLabel = new JLabel("Kies Plek"), schoonmaakLabel = new JLabel("Schoonmaken");
	private Object[] cValues, cValues2 = {"Ja", "nee"};
	private ParkeergarageGui gui;
	private JComboBox<Object> plekken, schoonmaken;
	private Garage g;
	
	public PlekVrijmakenGui(Garage g, ParkeergarageGui gui) {
		this.g = g;
		this.gui = gui;
		Container p = getContentPane();
		MigLayout layout = new MigLayout();
		
		ArrayList<Plek> dePlekken = g.getPlekken();
		cValues = dePlekken.toArray();
		plekken = new JComboBox<Object>(cValues);
		schoonmaken = new JComboBox<Object>(cValues2);
		
		
		setTitle("ATD -> Garage -> Plek Reserveren");
		setSize(350, 230);
		setDefaultCloseOperation(HIDE_ON_CLOSE);
		setLayout(layout);
		setLocationRelativeTo(null);
		
		p.setBackground(GLConfiguratie.getFrameBackground());
		p.setForeground(Color.GREEN);
		plekLabel.setForeground(GLConfiguratie.getvButtonForeground());
		schoonmaakLabel.setForeground(GLConfiguratie.getvButtonForeground());
		
		configureButton(aanpassen);
		

		add(plekLabel, "cell 0 0");
		add(plekken, "cell 1 0");
		add(schoonmaakLabel, "cell 0 1");
		add(schoonmaken, "cell 1 1");
		add(aanpassen, "cell 0 2, gaptop 20");
		
		
        setVisible(true);  
	}
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == aanpassen) {
			System.out.println("-> " + aanpassen.getText());
			Plek p = (Plek) plekken.getSelectedItem();
			boolean s = false;
			if(schoonmaken.getSelectedItem().equals("Ja")) {
				s = true;
			}
			g.plekVrijmaken(p, s);
			gui.refreshTable();
			JOptionPane.showMessageDialog(null, "Plek Vrijgemaakt");
			this.dispose();
		}
	}
	
	public void configureButton(JButton b) {
		b.setBackground(GLConfiguratie.getvButtonBackground());
		b.setForeground(GLConfiguratie.getvButtonForeground());
		b.setPreferredSize(new Dimension(100,30));
		b.addActionListener(this);
	}
}
