package nl.grootligt.gui;

import java.awt.*;
import java.awt.event.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.*;

import net.miginfocom.swing.MigLayout;
import nl.grootligt.klantenbinding.Klandizie;
import nl.grootligt.klantenbinding.Klant;
import nl.grootligt.parkeergarage.Auto;
import nl.grootligt.parkeergarage.Garage;
import nl.grootligt.parkeergarage.Plek;
import nl.grootligt.utils.GLConfiguratie;

public class PlekReserverenGui extends JFrame implements ActionListener {
	/**
	 * @author Berend de Groot
	 */
	private static final long serialVersionUID = 1L;
	private JButton aanpassen = new JButton("Reserveren");
	private JLabel klantLabel = new JLabel("Kies Klant"), datumLabel = new JLabel("Begin Datum"), plekLabel = new JLabel("Kies Plek"), autoLabel = new JLabel("Kies Auto");
	private JTextField datum = new JTextField(15);
	private Object[] cValues, cValues2, cValues3;
	private ParkeergarageGui gui;
	private JComboBox<?> klanten, plekken, autos;
	private Garage g;
	private Klandizie k;
	
	public PlekReserverenGui(Klandizie k, Garage g, ParkeergarageGui gui) {
		this.g = g;
		this.k = k;
		this.gui = gui;
		Container p = getContentPane();
		MigLayout layout = new MigLayout();
		
		ArrayList<Klant> deKlanten = k.getDeKlanten();
		cValues = deKlanten.toArray();
		klanten = new JComboBox<Object>(cValues);
		
		ArrayList<Plek> dePlekken = g.getPlekken();
		cValues2 = dePlekken.toArray();
		plekken = new JComboBox<Object>(cValues2);
		
		ArrayList<Auto> deAutos = g.getAutos();
		cValues3 = deAutos.toArray();
		autos = new JComboBox<Object>(cValues3);
		
		setTitle("ATD -> Garage -> Plek Reserveren");
		setSize(350, 230);
		setDefaultCloseOperation(HIDE_ON_CLOSE);
		setLayout(layout);
		setLocationRelativeTo(null);
		
		p.setBackground(GLConfiguratie.getFrameBackground());
		p.setForeground(Color.GREEN);
		klantLabel.setForeground(GLConfiguratie.getvButtonForeground());
		datumLabel.setForeground(GLConfiguratie.getvButtonForeground());
		plekLabel.setForeground(GLConfiguratie.getvButtonForeground());
		autoLabel.setForeground(GLConfiguratie.getvButtonForeground());
		
		configureButton(aanpassen);
		
		add(klantLabel, "cell 0 0");
		add(klanten, "cell 1 0");
		add(datumLabel, "cell 0 2 ");
		add(datum, "cell 1 2");
		add(plekLabel, "cell 0 3");
		add(plekken, "cell 1 3");
		add(autoLabel, "cell 0 4");
		add(autos, "cell 1 4");
		add(aanpassen, "cell 0 5, gaptop 20");
		
		
        setVisible(true);  
	}
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == aanpassen) {
			System.out.println("-> " + aanpassen.getText());
			if(!datum.equals("")) {
				Date date = null;
				try {
					date = new SimpleDateFormat("dd-MM-yyyy").parse(datum.getText());
				} catch (ParseException e1) {
					JOptionPane.showMessageDialog(null, "Datum graag in de volgende format: dd-MM-yyyy");
					return;
				}
				Plek p = (Plek) plekken.getSelectedItem();
				Auto a = (Auto) autos.getSelectedItem();
				g.plekReserveren(a, p, date);
				gui.refreshTable();
				JOptionPane.showMessageDialog(null, "Plek Gereserveerd");
				this.dispose();
			}
		}
	}
	
	public void configureButton(JButton b) {
		b.setBackground(GLConfiguratie.getvButtonBackground());
		b.setForeground(GLConfiguratie.getvButtonForeground());
		b.setPreferredSize(new Dimension(100,30));
		b.addActionListener(this);
	}
}
