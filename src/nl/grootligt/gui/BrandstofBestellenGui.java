package nl.grootligt.gui;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import net.miginfocom.swing.MigLayout;
import nl.grootligt.utils.GLConfiguratie;
import nl.grootligt.voorraadbeheer.Brandstof;
import nl.grootligt.voorraadbeheer.Magazijn;

public class BrandstofBestellenGui extends JFrame implements ActionListener {
	/**
	 * @author Berend de Groot
	 */
	private static final long serialVersionUID = 1L;
	private Magazijn m;
	private JLabel onderdelen = new JLabel("Brandstof"), aantal = new JLabel("Aantal"), vol = new JLabel("2 brandstoffen per keer.");
	private JButton bestel = new JButton("Bestel"), plus = new JButton("+");
	private ArrayList<Brandstof> deBrandstoffen;
	private Object[] cValues;
	private JComboBox<?> verschillendeOnderdelen1, verschillendeOnderdelen2;
	private JTextField onderdeel1 = new JTextField(10), onderdeel2 = new JTextField(10);
	private MigLayout layout = new MigLayout();
	private JPanel panel = new JPanel();
	private int aantalVelden = 1;
	private VoorraadBeheerGui j;
	
	
	public BrandstofBestellenGui(Magazijn m, VoorraadBeheerGui j) {
		this.m = m;
		this.j = j;
		deBrandstoffen = m.getDeBrandstoffen();
		cValues = deBrandstoffen.toArray();
		
		verschillendeOnderdelen1 = new JComboBox<Object>(cValues);
		verschillendeOnderdelen2 = new JComboBox<Object>(cValues);
		
		Container p = getContentPane();
		
		setTitle("ATD -> Voorraad Beheer -> Brandstoff Bestellen");
		setSize(240, 300);
		setDefaultCloseOperation(HIDE_ON_CLOSE);
		setLayout(layout);
		setLocationRelativeTo(null);
		setResizable(false);
		
		p.setBackground(GLConfiguratie.getFrameBackground());
		p.setForeground(Color.GREEN);
		onderdelen.setForeground(GLConfiguratie.getvButtonForeground());
		aantal.setForeground(GLConfiguratie.getvButtonForeground());
		panel.setBackground(GLConfiguratie.getFrameBackground());
		panel.setPreferredSize(new Dimension(200, 200));
		vol.setForeground(GLConfiguratie.getvButtonForeground());
		
		configureButton(bestel);
		configureButton(plus);
		plus.setPreferredSize(new Dimension(30,20));
		
		add(onderdelen, "gapright 50");
		add(aantal, "wrap 20");
		panel.add(verschillendeOnderdelen1);
		panel.add(onderdeel1);
		add(panel, "span 2 0, wrap 20");
		add(plus);
		add(bestel);
		
		setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == plus) {
			System.out.println("-> " + plus.getText());
			switch(aantalVelden) {
				case 1: panel.add(verschillendeOnderdelen2); panel.add(onderdeel2); break;
				case 2: panel.add(vol); plus.setEnabled(false); break;
			}
			aantalVelden++;
			panel.validate();
			panel.repaint();
			
		} else if(e.getSource() == bestel) {
			boolean b1 = false;
			
			if(!verschillendeOnderdelen1.getSelectedItem().equals("") && !onderdeel1.getText().equals("")) {
				Brandstof o = (Brandstof) verschillendeOnderdelen1.getSelectedItem();
				double a = Double.parseDouble(onderdeel1.getText());
				if(a < 0) {
					JOptionPane.showMessageDialog(null, "Het bedrag bij de eerste brandstof moet hoger als 0 zijn.");
					return;
				}
				b1 = m.addBrandstof(o.getType(), a);
			}
			if(!verschillendeOnderdelen2.getSelectedItem().equals("") && !onderdeel2.getText().equals("")) {
				Brandstof o = (Brandstof) verschillendeOnderdelen2.getSelectedItem();
				double a = Double.parseDouble(onderdeel2.getText());
				if(a < 0) {
					JOptionPane.showMessageDialog(null, "Het bedrag bij de tweede brandstof moet hoger als 0 zijn.");
					return;
				}
				m.addBrandstof(o.getType(), a);
			}
			if(b1) {
				JOptionPane.showMessageDialog(null, "Brandstof Besteld");
				this.dispose();
			} else {
				JOptionPane.showMessageDialog(null, "Brandstof niet Besteld");
				return;
			}
			j.refreshTable();
		}	
	}
	
	public void configureButton(JButton b) {
		b.setBackground(GLConfiguratie.getvButtonBackground());
		b.setForeground(GLConfiguratie.getvButtonForeground());
		b.setPreferredSize(new Dimension(100,30));
		b.addActionListener(this);
	}
}
